package fr.esipe.escape;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jbox2d.common.Vec2;

import fr.esipe.escape.game.Map;
import fr.esipe.escape.game.MouseFactory;
import fr.esipe.escape.game.activeWeapon.WeaponActive;
import fr.esipe.escape.game.bonus.Bonus;
import fr.esipe.escape.game.spaceship.Deplacement;
import fr.esipe.escape.game.spaceship.Player;
import fr.esipe.escape.game.spaceship.enemy.SpaceShipEnemy;
import fr.esipe.escape.game.weapon.Weapon;
import fr.esipe.escape.gesture.FactoryGesture;
import fr.esipe.escape.gesture.Gesture;
import fr.esipe.escape.level.BuildLevel;
import fr.esipe.escape.level.Level;
import fr.esipe.escape.ui.FPSActivity;
import fr.esipe.escape.ui.HighscoreActivity;
import fr.esipe.escape.ui.LifeActivity;
import fr.esipe.escape.ui.MenuActivity;
import fr.esipe.escape.ui.WeaponsActivity;
import fr.esipe.escape.ui.drawable.ScrollMap;
import fr.umlv.zen2.Application;
import fr.umlv.zen2.ApplicationCode;
import fr.umlv.zen2.ApplicationContext;
import fr.umlv.zen2.ApplicationRenderCode;
import fr.umlv.zen2.MotionEvent;

/**
 * This is the main program of Escape-IR.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class NewMainClass {

	private static GameState gameState = GameState.MENU;
	private static Image ground;
	private static Map world;
	private static int countPaint = 0;
	private static long millis = 0;
	private static int fps = -1;
	public static int WIDTH = 600;
	public static int HEIGHT = 600;
	private static Level my_level;
	private static BuildLevel build;

	private enum GameState {
		PLAY, MENU, WIN, LOSE, QUIT
	}

	public static void main(String[] args) throws Exception {
		Set.createSet(HEIGHT, WIDTH);
		world = new Map();
		build = new BuildLevel();
		
		Application.run("Escape-IR", WIDTH, HEIGHT, new ApplicationCode() {

			@Override
			public void run(ApplicationContext context) {
				Frame.getFrames()[0].setLocationRelativeTo(null);

				// Start the game.
				for (;;) {
					switch (gameState) {
					case PLAY:
						int nbLevel = build.getNblevel();
						for (int i = 1; i <= nbLevel; i++) {
							newMap(i);
							play(context, my_level);
						}
						break;
					case MENU:
						displayMenu(context);
						break;
					}
				}

			}

		});
	}

	private static void newMap(int level) {
		// Create world (walls, spaceships, start, finish)
		my_level = build.getLevel(level);
		ground = my_level.getBackground();
		gameState = GameState.PLAY;
	}

	private static void play(ApplicationContext context, Level my_Level) {

		// Set horizontal speed for each layer.
		float[] vy = { -0.09f };
		// Set y position for each layer.
		float[] y = { getHEIGHT() };
		// Gap for each layer.
		float[] gap = { 0f };
		Image layers[] = new Image[1];
		layers[0] = ground;
		long nextFrameStart = System.nanoTime();
		long FRAME_PERIOD = 1000000000L / 60;

		// Create a parallax engine.
		final ScrollMap scrollMap = new ScrollMap(getHEIGHT(), 10 * 0.15f,
				layers, vy, y, gap);

		final Player sp = my_level.getPlayer();
		Map.addPlayer(sp);
		final List<Vec2> list = new ArrayList<>();
		boolean armed = false;
		boolean finishBoss = false;
		boolean launchBoss = false;
		while (!finishBoss) {
			boolean finalGesture = false;
			long elapsedTime = 0;
			MotionEvent event = null;
			do {
				event = context.pollMotion();
				if (event != null) {
					list.add(new Vec2(event.getX(), event.getY()));
					if (event.toString().contains("ACTION_UP")) {
						finalGesture = true;
						int mouseX = event.getX(), mouseY = event.getY();
						if (mouseX > 550 && mouseX < WIDTH && mouseY > 150
								&& mouseY < 350) {
							// Appel d'une m�thode dans MouseFactory.
							Map.setWeaponActive(MouseFactory
									.eventOnWeapon(event));
						}
					}
				}
				// Tell the engine to move layers to the right.
				scrollMap.setMoveTop();
				// Actually performs the movement.
				scrollMap.move();
				nextFrameStart += FRAME_PERIOD;

			} while (nextFrameStart < System.nanoTime()
					&& finalGesture == false
					&& (elapsedTime <= 2500 || event != null));

			context.render(new ApplicationRenderCode() {
				@Override
				public void render(Graphics2D graphics) {
					world.updateMap();
					graphics.setColor(Color.BLACK);
					scrollMap.render(graphics);
					long currentMillis = System.currentTimeMillis();
					if (countPaint == 0) {
						millis = System.currentTimeMillis();
						countPaint = 1;
					} else if (System.currentTimeMillis() - millis < 1000) {
						countPaint++;
					} else {
						estimateFPS(currentMillis);
					}
					FPSActivity.draw(graphics);
					HighscoreActivity.draw(graphics);
					LifeActivity.draw(graphics);
					WeaponsActivity.draw(graphics, Map.getWeaponActive());
					if (sp.isAlive()) {
						sp.draw(graphics);
					}
					if (FactoryGesture.GestureFactory(list) != null) {
						graphics.setColor(Color.GREEN);
					} else {
						graphics.setColor(Color.RED);
					}
					for (Vec2 vec : list) {

						graphics.drawRect((int) (vec.x - sp.getBody()
								.getPosition().x), (int) (vec.y - sp.getBody()
								.getPosition().y), 2, 2);

					}

					for (SpaceShipEnemy sse : Map.getSpaceshipEnemyList()) {
						if (sse.isAlive()) {
							sse.draw(graphics);
							if (sse.getNbMissile() > 0) {
								Weapon w = sse.getNextMissile();
								Map.getWeaponsList().add(w);
							}
						}
					}

					for (Weapon w : Map.getWeaponsList()) {
						w.draw(graphics);
					}

					for (Bonus bonus : Map.getBonusList()) {
						bonus.draw(graphics);
					}
				}
			});

			if (!list.isEmpty() && finalGesture == true) {
				armed = WeaponActive.detectGesture(list.get(0), sp);
				if (armed == true) {
					Vec2 path = WeaponActive.calculPath(list, sp);
					if (path.x != 0 && path.y != 0) {
						Map.shootWeaponFromPlayer(Map.getWeaponActive(), path,
								sp);
						armed = false;
					}
				}

				else {
					Gesture detected_Gesture = FactoryGesture
							.GestureFactory(list);
					if (detected_Gesture != null) {
						for (int i = 0; i < list.size(); i++) {
							Vec2 vec = list.get(i);
							vec.x = vec.x - sp.getBody().getPosition().x;
							vec.y = vec.y - sp.getBody().getPosition().y;
							list.set(i, vec);
						}
						detected_Gesture.calculIntensity(list, new Vec2(0, 0));
						Deplacement deplacement = detected_Gesture.calculForce(
								new Vec2(sp.getBody().getPosition().x, sp
										.getBody().getPosition().y),
								detected_Gesture.getIntensity(), sp
										.getElement());
						sp.setMovement(deplacement);
						sp.setVec2(deplacement.getForce());
					}

				}

				list.clear();
			}

			if (Map.getSpaceshipEnemyList().isEmpty()) {
				ArrayList<SpaceShipEnemy> tmp = my_Level.selectionEnnemis();
				if (tmp == null) {
					Map.getSpaceshipEnemyList().clear();
					Map.getSpaceshipEnemyList().add(my_Level.getBoss());
					launchBoss = true;
				} else {
					Map.getSpaceshipEnemyList().clear();
					Map.getSpaceshipEnemyList().addAll(tmp);
				}
			} else {
				Iterator<SpaceShipEnemy> iter = Map.getSpaceshipEnemyList()
						.iterator();
				while (iter.hasNext()) {
					SpaceShipEnemy enemy = iter.next();
					if (enemy.remove()) {
						iter.remove();
					}
				}

			}

			if (launchBoss == true
					&& !Map.getSpaceshipEnemyList()
							.contains(my_Level.getBoss())) {
				finishBoss = true;
				Map.setWIN(true);
			}

			if (Map.isWIN()) {
				displayWin(context);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.err.println("InterruptedException !");
				}
				Map.setWIN(false);
				displayMenu(context);
			}

			if (Map.isLOSE()) {
				displayLose(context);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.err.println("InterruptedException !");
				}
				Map.setLOSE(false);
				displayMenu(context);
			}

			long remaining = nextFrameStart - System.nanoTime();
			if (remaining > 0) {
				try {
					Thread.sleep(remaining / 1000000);
				} catch (Throwable t) {
				}
			}
		}
	}

	private static void displayMenu(ApplicationContext context) {

		for (;;) {
			final MotionEvent event = context.pollMotion();
			if (event != null) {
				int choice = MouseFactory.eventOnMenuActivity(event);
				switch (choice) {
				case 1:
					gameState = GameState.MENU;
					displayLevel(context);
					break;
				case 2:
					gameState = GameState.MENU;
					displayOptions(context);
				case 3:
					gameState = GameState.MENU;
					displayCredits(context);
				default:
					break;
				}
			}
			context.render(new ApplicationRenderCode() {
				@Override
				public void render(Graphics2D graphics) {
					MenuActivity.drawMenu(graphics);

				}
			});
		}
	}

	private static void displayLevel(ApplicationContext context) {
		for (;;) {
			final MotionEvent event = context.pollMotion();
			if (event != null) {
				int level = MouseFactory.eventOnMenuActivityLevel(event);
				switch (level) {
				case 1:
				case 2:
				case 3:
					newMap(level);
					play(context, my_level);
					gameState = GameState.PLAY;
					break;
				case 4:
					displayMenu(context);
					gameState = GameState.MENU;
				default:
					break;
				}
			}

			context.render(new ApplicationRenderCode() {
				@Override
				public void render(Graphics2D graphics) {
					MenuActivity.drawLevel(graphics);

				}
			});
		}
	}

	private static void displayOptions(ApplicationContext context) {
		for (;;) {
			final MotionEvent event = context.pollMotion();
			if (event != null) {
				if (MouseFactory.eventOnMenuActivityOptions(event)) {
					displayMenu(context);
					gameState = GameState.MENU;
				}
			}

			context.render(new ApplicationRenderCode() {
				@Override
				public void render(Graphics2D graphics) {
					MenuActivity.drawOptions(graphics);

				}
			});
		}
	}

	private static void displayCredits(ApplicationContext context) {
		for (;;) {
			final MotionEvent event = context.pollMotion();
			if (event != null) {
				if (MouseFactory.eventOnMenuActivityOptions(event)) {
					displayMenu(context);
					gameState = GameState.MENU;
				}
			}

			context.render(new ApplicationRenderCode() {
				@Override
				public void render(Graphics2D graphics) {
					MenuActivity.drawCredits(graphics);

				}
			});
		}
	}

	private static void displayWin(ApplicationContext context) {

		context.render(new ApplicationRenderCode() {
			@Override
			public void render(Graphics2D graphics) {
				MenuActivity.drawWin(graphics);

			}
		});
	}

	private static void displayLose(ApplicationContext context) {

		context.render(new ApplicationRenderCode() {
			@Override
			public void render(Graphics2D graphics) {
				MenuActivity.drawLose(graphics);

			}
		});
	}

	private static int estimateFPS(long currentMillis) {
		float tmp = ++countPaint;
		fps = (int) (tmp / (currentMillis - millis) * 1000);
		millis = currentMillis;
		countPaint = 0;
		return fps;
	}

	public static int getFPS() {
		return fps;
	}

	public static int getWIDTH() {
		return WIDTH;
	}

	public static int getHEIGHT() {
		return HEIGHT;
	}

}
