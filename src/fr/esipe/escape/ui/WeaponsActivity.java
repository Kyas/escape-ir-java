package fr.esipe.escape.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;

import fr.esipe.escape.Set;
import fr.esipe.escape.game.Map;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * This class displays the Weapons bar in the game.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class WeaponsActivity {

	/**
	 * The width of the weapon bar.
	 */
	private static int WIDTH = 50;

	/**
	 * The height of the weapon bar.
	 */
	private static int HEIGHT = 200;

	private static Image weaponMissile = ImageFactory
			.getImage("weaponActivity-missile.png");
	private static Image weaponFireball = ImageFactory
			.getImage("weaponActivity-fireball.png");
	private static Image weaponShiboleet = ImageFactory
			.getImage("weaponActivity-shiboleet.png");
	private static Image weaponLaser = ImageFactory
			.getImage("weaponActivity-laser.png");

	public static void draw(Graphics2D graphics, int weaponActive) {

		int missileAmmo = Map.getWeaponsTable().get("Missile").getNbAmmos();
		int fireballAmmo = Map.getWeaponsTable().get("Fireball").getNbAmmos();
		int shiboleetAmmo = Map.getWeaponsTable().get("Shiboleet").getNbAmmos();
		int laserAmmo = Map.getWeaponsTable().get("Laser").getNbAmmos();

		AffineTransform at = new AffineTransform();
		at.setToIdentity();
		graphics.setTransform(at);
		graphics.setColor(Color.BLACK);
		switch (weaponActive) {
		case 0:
			graphics.drawImage(weaponMissile,
					(int) Set.getSet().getWidth() - 50, (int) Set.getSet()
							.getHeight() / 2 - 150, WIDTH, HEIGHT, null);
			break;
		case 1:
			graphics.drawImage(weaponFireball,
					(int) Set.getSet().getWidth() - 50, (int) Set.getSet()
							.getHeight() / 2 - 150, WIDTH, HEIGHT, null);
			break;
		case 2:
			graphics.drawImage(weaponShiboleet,
					(int) Set.getSet().getWidth() - 50, (int) Set.getSet()
							.getHeight() / 2 - 150, WIDTH, HEIGHT, null);
			break;
		case 3:
			graphics.drawImage(weaponLaser, (int) Set.getSet().getWidth() - 50,
					(int) Set.getSet().getHeight() / 2 - 150, WIDTH, HEIGHT,
					null);
			break;
		default:
			graphics.drawImage(weaponMissile,
					(int) Set.getSet().getWidth() - 50, (int) Set.getSet()
							.getHeight() / 2 - 150, WIDTH, HEIGHT, null);
		}

		Font f = new Font("Verdana", Font.ROMAN_BASELINE | Font.BOLD, 1);
		Font bigfont = f.deriveFont(AffineTransform
				.getScaleInstance(12.0, 12.0));

		/**
		 * Ammos for missile.
		 */
		GlyphVector gv = null;
		if (missileAmmo > 0) {
			gv = bigfont.createGlyphVector(graphics.getFontRenderContext(),
					String.valueOf(missileAmmo));
		} else {
			gv = bigfont
					.createGlyphVector(graphics.getFontRenderContext(), "0");
		}
		Shape jshape = gv.getOutline();
		graphics.translate((int) Set.getSet().getWidth() - 37, (int) Set
				.getSet().getHeight() / 2 - 103);
		graphics.setPaint(Color.WHITE);
		graphics.fill(jshape);
		if (missileAmmo > 0) {
			graphics.setPaint(Color.BLACK);
		} else {
			graphics.setPaint(Color.RED);
		}
		graphics.draw(jshape);

		at.setToIdentity();
		graphics.setTransform(at);
		/**
		 * Ammos for fireball.
		 */
		GlyphVector gv2 = null;
		if (fireballAmmo > 0) {
			gv2 = bigfont.createGlyphVector(graphics.getFontRenderContext(),
					String.valueOf(fireballAmmo));
		} else {
			gv2 = bigfont.createGlyphVector(graphics.getFontRenderContext(),
					"0");
		}
		Shape jshape2 = gv2.getOutline();
		graphics.translate((int) Set.getSet().getWidth() - 33, (int) Set
				.getSet().getHeight() / 2 - 57);
		graphics.setPaint(Color.WHITE);
		graphics.fill(jshape2);
		if (fireballAmmo > 0) {
			graphics.setPaint(Color.BLACK);
		} else {
			graphics.setPaint(Color.RED);
		}
		graphics.draw(jshape2);

		at.setToIdentity();
		graphics.setTransform(at);
		/**
		 * Ammos for shiboleet.
		 */
		GlyphVector gv3 = null;
		if (shiboleetAmmo > 0) {
			gv3 = bigfont.createGlyphVector(graphics.getFontRenderContext(),
					String.valueOf(shiboleetAmmo));
		} else {
			gv3 = bigfont.createGlyphVector(graphics.getFontRenderContext(),
					"0");
		}
		Shape jshape3 = gv3.getOutline();
		graphics.translate((int) Set.getSet().getWidth() - 30, (int) Set
				.getSet().getHeight() / 2 - 8);
		graphics.setPaint(Color.WHITE);
		graphics.fill(jshape3);
		if (shiboleetAmmo > 0) {
			graphics.setPaint(Color.BLACK);
		} else {
			graphics.setPaint(Color.RED);
		}
		graphics.draw(jshape3);

		at.setToIdentity();
		graphics.setTransform(at);
		/**
		 * Ammos for laser.
		 */
		GlyphVector gv4 = null;
		if (laserAmmo > 0) {
			gv4 = bigfont.createGlyphVector(graphics.getFontRenderContext(),
					String.valueOf(laserAmmo));
		} else {
			gv4 = bigfont.createGlyphVector(graphics.getFontRenderContext(),
					"0");
		}
		Shape jshape4 = gv4.getOutline();
		graphics.translate((int) Set.getSet().getWidth() - 30, (int) Set
				.getSet().getHeight() / 2 + 40);
		graphics.setPaint(Color.WHITE);
		graphics.fill(jshape4);
		if (laserAmmo > 0) {
			graphics.setPaint(Color.BLACK);
		} else {
			graphics.setPaint(Color.RED);
		}
		graphics.draw(jshape4);
	}

}
