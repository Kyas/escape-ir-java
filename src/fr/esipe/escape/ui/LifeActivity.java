package fr.esipe.escape.ui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;

import fr.esipe.escape.game.spaceship.Player;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * LifeActivity - Displays the life of the player.
 * 
 * @author C�line Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 */
public class LifeActivity {

	/**
	 * The static width's activity.
	 */
	private static int WIDTH = 150;

	/**
	 * The static height's activity.
	 */
	private static int HEIGHT = 30;

	/**
	 * The image when the life's player got 10 hearts.
	 */
	private static Image life10 = ImageFactory.getImage("lifeActivity10.png");

	/**
	 * The image when the life's player got 9 hearts.
	 */
	private static Image life9 = ImageFactory.getImage("lifeActivity9.png");

	/**
	 * The image when the life's player got 8 hearts.
	 */
	private static Image life8 = ImageFactory.getImage("lifeActivity8.png");

	/**
	 * The image when the life's player got 7 hearts.
	 */
	private static Image life7 = ImageFactory.getImage("lifeActivity7.png");

	/**
	 * The image when the life's player got 6 hearts.
	 */
	private static Image life6 = ImageFactory.getImage("lifeActivity6.png");

	/**
	 * The image when the life's player got 5 hearts.
	 */
	private static Image life5 = ImageFactory.getImage("lifeActivity5.png");

	/**
	 * The image when the life's player got 4 hearts.
	 */
	private static Image life4 = ImageFactory.getImage("lifeActivity4.png");

	/**
	 * The image when the life's player got 3 hearts.
	 */
	private static Image life3 = ImageFactory.getImage("lifeActivity3.png");

	/**
	 * The image when the life's player got 2 hearts.
	 */
	private static Image life2 = ImageFactory.getImage("lifeActivity2.png");

	/**
	 * The image when the life's player got 1 hearts.
	 */
	private static Image life1 = ImageFactory.getImage("lifeActivity1.png");

	/**
	 * The image when the life's player is empty.
	 */
	private static Image life0 = ImageFactory.getImage("lifeActivity0.png");

	/**
	 * Display the LifeActivity. It show different images of life depending to
	 * the player's life in the game.
	 * 
	 * @param graphics
	 *            graphics from Zen2.
	 */
	public static void draw(Graphics2D graphics) {

		int life = Player.getLife();

		AffineTransform at = new AffineTransform();
		at.setToIdentity();
		graphics.setTransform(at);
		graphics.setColor(Color.BLACK);
		if (life > 10) {
			graphics.drawImage(life10, 0, 20, WIDTH, HEIGHT, null);
		} else {
			switch (life) {
			case 10:
				graphics.drawImage(life10, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 9:
				graphics.drawImage(life9, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 8:
				graphics.drawImage(life8, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 7:
				graphics.drawImage(life7, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 6:
				graphics.drawImage(life6, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 5:
				graphics.drawImage(life5, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 4:
				graphics.drawImage(life4, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 3:
				graphics.drawImage(life3, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 2:
				graphics.drawImage(life2, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 1:
				graphics.drawImage(life1, 0, 20, WIDTH, HEIGHT, null);
				break;
			case 0:
				graphics.drawImage(life0, 0, 20, WIDTH, HEIGHT, null);
				break;
			default:
				graphics.drawImage(life0, 0, 20, WIDTH, HEIGHT, null);
			}
		}

	}

}
