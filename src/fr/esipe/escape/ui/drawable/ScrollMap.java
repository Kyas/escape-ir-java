package fr.esipe.escape.ui.drawable;

import java.awt.Graphics2D;
import java.awt.Image;

/**
 * This class is used to do a Parallax scrolling for our differents maps.
 * 
 * @author C�line Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 */
public class ScrollMap {

	/** Layers used to execute parallax scrolling. */
	private Layer[] layers;

	/** Number of layers. */
	private int numLayers;

	/**
	 * Creates a new parallax engine.
	 * 
	 * @param pScreenHeight
	 *            Screen height in pixels.
	 * @param pDy
	 *            Movement factor in pixels.
	 * @param pImages
	 *            Images used to compose parallax layers.
	 * @param pVy
	 *            Velocity for layers movement.
	 * @param gap
	 *            Gap between layers.
	 */
	public ScrollMap(int pScreenHeight, float pDy, Image[] pImages,
			float[] pVy, float[] gap) {
		this(pScreenHeight, pDy, pImages, pVy, new float[pImages.length], gap);
	}

	/**
	 * Creates a new parallax scrolling engine.
	 * 
	 * @param pScreenHeight
	 *            Screen width in pixels.
	 * @param pDy
	 *            Movement factor in pixels.
	 * @param pImages
	 *            Images used to compose parallax layers.
	 * @param pVy
	 *            Velocity for layers movement.
	 * @param pY
	 *            Y position to render the image layers.
	 */
	public ScrollMap(int pScreenHeight, float pDy, Image[] pImages,
			float[] pVy, float[] pY, float[] gap) {
		this.numLayers = pImages.length;
		this.layers = new Layer[this.numLayers];
		for (int i = 0; i < this.numLayers; i++) {
			this.layers[i] = new Layer(pScreenHeight, 0, pY[i], pImages[i],
					pVy[i] * pDy, gap[i]);
		}
	}

	/**
	 * Move layers to the top.
	 */
	public void setMoveTop() {
		for (int i = 0; i < this.numLayers; i++) {
			this.layers[i].setTopDirection();
		}
	}

	/**
	 * Stop layers movement.
	 */
	public void stop() {
		for (int i = 0; i < this.numLayers; i++) {
			this.layers[i].stop();
		}
	}

	/**
	 * Move layers.
	 */
	public void move() {
		for (int i = 0; i < this.numLayers; i++) {
			this.layers[i].move();
		}
	}

	/**
	 * The display order is important. Display layers from the back to the front
	 * of the scene.
	 * 
	 * @param g
	 *            Graphics context.
	 */
	public void render(Graphics2D g) {
		for (int i = 0; i < this.numLayers; i++) {
			this.layers[i].render(g);
		}
	}

	/**
	 * Represents an vertical parallax layer.
	 * 
	 */
	private class Layer {

		/** Image for the layer. */
		private Image image;

		/** Width of the image in pixels. */
		private int width;

		/** Height of the image in pixels. */
		private int height;

		/** Height of the screen in pixels. */
		private int screenHeight;

		/**
		 * X position in screen where the bottom side of the image should be
		 * drawn.
		 */
		private float x;

		/**
		 * Y position in screen where the bottom side of the image should be
		 * drawn.
		 */
		private float y;

		/** Number of pixels the layer will move with the height. */
		private float dy;

		/** Gap between layers. */
		private float gap;

		/** Flag indicating if the layer is moving to the top. */
		private boolean isMovingTop;

		/**
		 * Creates a new layer at origin (0,0).
		 * 
		 * @param pScreenHeight
		 *            Screen height in pixels.
		 * @param pImage
		 *            Image for the layer.
		 * @param pDy
		 *            Movement factor in pixels.
		 */
		private Layer(int pScreenHeight, Image pImage, float pDy) {
			this(pScreenHeight, 0, 0, pImage, pDy, 0);
		}

		/**
		 * Creates a new layer in a specific (x,y) position.
		 * 
		 * @param pScreenHeight
		 *            Screen width in pixels.
		 * @param pX
		 *            The x position in the screen where the start of the image
		 *            should be drawn.
		 * @param pY
		 *            The y position in the screen where the start of the image
		 *            should be drawn. It can range between -height to height-1
		 *            , so can have a value beyond the confines of the screen
		 *            (0-pScreenHeight). <br>
		 *            As y varies, the on-screen layer will usually be a
		 *            combination of its tail followed by its head.
		 * @param pImage
		 *            Image for the layer.
		 * @param pDy
		 *            Movement factor in pixels.
		 */
		private Layer(int pScreenHeight, float pX, float pY, Image pImage,
				float pDy, float gap) {
			this.screenHeight = pScreenHeight;
			this.image = pImage;
			this.width = pImage.getWidth(null);
			this.height = pImage.getHeight(null);
			this.dy = pDy;
			this.isMovingTop = false;
			this.x = pX;
			this.y = pY;
			this.gap = gap;
		}

		/**
		 * Make the layer move to the top.
		 */
		private void setTopDirection() {
			this.isMovingTop = true;
		}

		/**
		 * Stop layer movement.
		 */
		private void stop() {
			this.isMovingTop = false;
		}

		/**
		 * Increment the y value depending on the movement flags. It can range
		 * between -height to height-1, which is the height of the image.
		 */
		private void move() {
			if (isMovingTop)
				this.y = (this.y - this.dy) % (this.height - gap);
		}

		/**
		 * Performs the layer rendering.
		 * 
		 * @param g
		 *            Graphics context.
		 */
		private void render(Graphics2D g) {
			if (y == 0) {
				// Draws image head at (0,0).
				draw(g, screenHeight, 0, 0, screenHeight);
			} else if ((y > 0) && (y < screenHeight)) {
				// Draws image tail at (0,0) and image head at (y,0).
				// Tail.
				draw(g, 0, y, height - y, height);
				// Head.
				draw(g, y, screenHeight, 0, screenHeight - y);
			} else if (y >= screenHeight) {
				// Draws only image tail at (0,0).
				draw(g, 0, screenHeight, height - y, height - y + screenHeight);
			} else if ((y < 0) && (y >= screenHeight - height)) {
				// Draws only image body.
				draw(g, 0, screenHeight, -y, screenHeight - y);
			} else if (y < screenHeight - height) {
				// Draws image tail at (0,0) and image head at (height+y,0)
				// Tail.
				draw(g, 0, height + y, -y, height);
				// Head.
				draw(g, gap + height + y, gap + screenHeight, 0, screenHeight
						- height - y);
			}
		}

		/**
		 * Actually draws the layer.
		 * 
		 * @param g
		 *            Graphics context.
		 * @param destY1
		 *            Destination Y1 position in the graphics context.
		 * @param destY2
		 *            Destination Y2 position in the graphics context.
		 * @param imageY1
		 *            Source Y1 position of image's layer.
		 * @param imageY2
		 *            Source Y2 position of image's layer.
		 */
		private void draw(Graphics2D g, float destY1, float destY2,
				float imageY1, float imageY2) {
			g.drawImage(image, (int) (x), (int) (destY1), (int) (x + width),
					(int) (destY2), 0, (int) (imageY1), (int) (width),
					(int) (imageY2), null);
		}
	}
}
