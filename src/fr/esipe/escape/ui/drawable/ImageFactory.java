package fr.esipe.escape.ui.drawable;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import javax.imageio.ImageIO;

/**
 * This class contains a factory which contains all images we need during the
 * game.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class ImageFactory {

	/**
	 * Contains all loaded images
	 */
	private final static Map<String, Image> images = new HashMap<String, Image>();

	/**
	 * Give a path and the corresponding image
	 * 
	 * @param path
	 *            is the generally the filename
	 * @return the image corresponding to the path
	 */
	public static Image getImage(String path) {
		Objects.requireNonNull(path);
		Image image = images.get(path);
		if (image == null) {
			File f = new File("resources/" + path);
			try {
				image = ImageIO.read(f);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			images.put(path, Objects.requireNonNull(image));
		}
		return image;
	}
}