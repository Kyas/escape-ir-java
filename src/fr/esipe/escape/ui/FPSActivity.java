package fr.esipe.escape.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;

import fr.esipe.escape.NewMainClass;

/**
 * FPSActivity - Displays the FPS of the game.
 * 
 * @author C�line Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 */
public class FPSActivity {

	/**
	 * Draw the activity.
	 * @param graphics The graphics from Zen2d.
	 */
	public static void draw(Graphics2D graphics) {
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		graphics.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);

		Font f = new Font("Verdana", Font.ROMAN_BASELINE | Font.BOLD, 2);

		Font bigfont = f.deriveFont(AffineTransform
				.getScaleInstance(10.0, 10.0));
		GlyphVector gv = bigfont.createGlyphVector(
				graphics.getFontRenderContext(), "FPS=" + NewMainClass.getFPS());
		Shape jshape = gv.getOutline();
		graphics.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_BEVEL));

		graphics.translate(NewMainClass.getWIDTH() - 100, 20);

		graphics.setPaint(Color.RED); // Fill with solid,opaque, red
		graphics.fill(jshape); // Fill the shape
		graphics.setPaint(Color.BLACK); // Switch to solid black
		graphics.draw(jshape); // And draw the outline
	}

}
