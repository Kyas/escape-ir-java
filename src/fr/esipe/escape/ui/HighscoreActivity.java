package fr.esipe.escape.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;

import fr.esipe.escape.game.Map;

/**
 * HighscoreActivity - Displays the Highscore bar in the game.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class HighscoreActivity {

	/**
	 * Display the HighscoreActivity.
	 * 
	 * @param graphics
	 *            graphics from Zen2.
	 */
	public static void draw(Graphics2D graphics) {
		int highscore = Map.getHIGHSCORE();
		
		AffineTransform at = new AffineTransform();
		at.setToIdentity();
		graphics.setTransform(at);
		graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		graphics.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);

		Font f = new Font("Verdana", Font.ROMAN_BASELINE | Font.BOLD, 2);

		Font bigfont = f.deriveFont(AffineTransform
				.getScaleInstance(10.0, 10.0));
		GlyphVector gv = bigfont.createGlyphVector(
				graphics.getFontRenderContext(), "Highscore : " + highscore);
		Shape jshape = gv.getOutline();
		graphics.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_ROUND,
				BasicStroke.JOIN_BEVEL));

		// Move to the baseline.
		graphics.translate(5, 20);

		graphics.setPaint(Color.WHITE); // Fill with solid, opaque white
		graphics.fill(jshape); // Fill the shape
		graphics.setPaint(Color.BLACK); // Switch to solid black
		graphics.draw(jshape); // And draw the outline

	}

}
