package fr.esipe.escape.ui;

import java.awt.Color;
import java.awt.Graphics2D;

import fr.esipe.escape.Set;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * This class displays the Menu bar before the game starts.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class MenuActivity {

	/**
	 * Draw the menu.
	 * 
	 * @param graphics
	 *            The graphics.
	 */
	public static void drawMenu(Graphics2D graphics) {
		graphics.setBackground(Color.BLACK);
		graphics.setColor(Color.BLACK);
		graphics.drawImage(ImageFactory.getImage("EscapeMenu.png"), 0, 0,
				(int) Set.getSet().getWidth(), (int) Set.getSet().getHeight(),
				null);

	}

	/**
	 * Draw the level.
	 * 
	 * @param graphics
	 *            The graphics.
	 */
	public static void drawLevel(Graphics2D graphics) {
		graphics.setBackground(Color.BLACK);
		graphics.setColor(Color.BLACK);
		graphics.drawImage(ImageFactory.getImage("EscapeMenuSelectWorld.png"),
				0, 0, (int) Set.getSet().getWidth(), (int) Set.getSet()
						.getHeight(), null);

	}

	/**
	 * Draw the options.
	 * 
	 * @param graphics
	 *            The graphics.
	 */
	public static void drawOptions(Graphics2D graphics) {
		graphics.setBackground(Color.BLACK);
		graphics.setColor(Color.BLACK);
		graphics.drawImage(ImageFactory.getImage("EscapeMenuOptions.png"), 0,
				0, (int) Set.getSet().getWidth(), (int) Set.getSet()
						.getHeight(), null);

	}

	/**
	 * Draw the credits.
	 * 
	 * @param graphics
	 *            The graphics.
	 */
	public static void drawCredits(Graphics2D graphics) {
		graphics.setBackground(Color.BLACK);
		graphics.setColor(Color.BLACK);
		graphics.drawImage(ImageFactory.getImage("EscapeMenuCredits.png"), 0,
				0, (int) Set.getSet().getWidth(), (int) Set.getSet()
						.getHeight(), null);

	}

	/**
	 * Draw the victory.
	 * 
	 * @param graphics
	 *            The graphics.
	 */
	public static void drawWin(Graphics2D graphics) {
		graphics.setBackground(Color.BLACK);
		graphics.setColor(Color.BLACK);
		graphics.drawImage(ImageFactory.getImage("victory.png"), 0, 0,
				(int) Set.getSet().getWidth(), (int) Set.getSet().getHeight(),
				null);

	}

	/**
	 * Draw the defeat.
	 * 
	 * @param graphics
	 *            The graphics.
	 */
	public static void drawLose(Graphics2D graphics) {
		graphics.setBackground(Color.BLACK);
		graphics.setColor(Color.BLACK);
		graphics.drawImage(ImageFactory.getImage("GameOver.png"), 0, 0,
				(int) Set.getSet().getWidth(), (int) Set.getSet().getHeight(),
				null);

	}

}
