package fr.esipe.escape.game;

import org.jbox2d.common.Vec2;

/**
 * This interface contains movements for our Weapons.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public interface Movable {

	/**
	 * If the body is movable.
	 * 
	 * @param force
	 *            The force.
	 */
	void move(Vec2 force);

}
