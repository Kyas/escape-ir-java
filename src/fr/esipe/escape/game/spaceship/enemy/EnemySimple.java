package fr.esipe.escape.game.spaceship.enemy;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.LinkedList;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.Set;
import fr.esipe.escape.collisions.Mask;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.Map;
import fr.esipe.escape.game.spaceship.BehaviourShip;
import fr.esipe.escape.game.spaceship.SpaceShip;
import fr.esipe.escape.game.weapon.Missile;
import fr.esipe.escape.game.weapon.Weapon;
import fr.esipe.escape.ui.drawable.ImageFactory;

public class EnemySimple extends SpaceShipEnemy implements BehaviourShip {

	private long frequence=5000;
	private long startTime=0;
	private PolygonShape shapeDef;
	private static int WIDTH = 47;
	private static int HEIGHT = 62;

	public EnemySimple(int x, int y) {
		super(x, y, ImageFactory.getImage("enemy-simple.png"),new LinkedList<Weapon>(), 1);
		shapeDef = new PolygonShape();
		shapeDef.setAsBox(WIDTH / 4, HEIGHT / 4);

		getBodyDef().position = new Vec2(x, y);
		getBodyDef().userData = this;
		getFixtureDef().shape = shapeDef;
		getFixtureDef().density = 0.f;
		getFixtureDef().friction = 0.1f;
		getFixtureDef().restitution = 0.1f;

		getFixtureDef().filter.categoryBits = Mask.CATEGORY_ENEMIES;
		getFixtureDef().isSensor = true;
		getFixtureDef().filter.categoryBits = Mask.CATEGORY_ENEMIES;
		getFixtureDef().filter.maskBits = Mask.MASK_ENEMIES;
		getFixtureDef().userData = EntityType.ENEMIES;
		setBody(Map.getWorld().createBody(getBodyDef()));
		getBody().createFixture(getFixtureDef());
		

	}

	public void update() {
		long actualTime = System.currentTimeMillis();
		if (actualTime - startTime > frequence) {
			Vec2 force;
			if(getBody().getPosition().x>Set.getSet().getWidth()/5){
				force=new Vec2(-20,50);
			}
			else{
				force=new Vec2(20,50);
			}
			getWeaponMissile().add(new Missile(new Vec2(getBody().getPosition().x+23, getBody().getPosition().y+62), force,false));
			startTime=actualTime;
		}

	}

	public void move(int x, int y) {
		Vec2 force = new Vec2(0, 30);
		getBody().setLinearVelocity(force);
	}

	@Override
	public void draw(Graphics2D graphics) {
		update();
		SpaceShip.resetTrans(graphics);
		graphics.translate(getBody().getPosition().x, getBody().getPosition().y);
		graphics.setColor(Color.WHITE);
		graphics.drawImage(getImg(), WIDTH/2 , -HEIGHT/2, WIDTH, HEIGHT, null);
		move(0,0);
	}
	
}
