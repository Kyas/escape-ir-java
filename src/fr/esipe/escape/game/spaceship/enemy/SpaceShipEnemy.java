package fr.esipe.escape.game.spaceship.enemy;

import java.awt.Graphics2D;
import java.awt.Image;
import java.util.LinkedList;
import java.util.Random;

import org.jbox2d.common.Vec2;

import fr.esipe.escape.Set;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.spaceship.BehaviourShip;
import fr.esipe.escape.game.spaceship.SpaceShip;
import fr.esipe.escape.game.weapon.Weapon;

public abstract class SpaceShipEnemy extends SpaceShip implements BehaviourShip{

	private LinkedList<Weapon> weaponMissile;
	private int nbLife;
	private boolean isAlive;

	private final int bonus;
	
	public SpaceShipEnemy(int x, int y, Image img , LinkedList<Weapon> list_w ,int nbLife) {
		super(x, y, img);
		this.weaponMissile = list_w;
		this.nbLife = nbLife;
		isAlive = true;
		
		getFixtureDef().userData = EntityType.ENEMIES;

		this.weaponMissile=list_w;
		this.nbLife=nbLife;
		this.nbLife = nbLife;
		
		boolean putBonus=new Random().nextBoolean();
		if(putBonus==false){
			bonus=-1;
		}
		else{
			bonus=Math.abs(new Random().nextInt()%3);
		}
	}

	public int getNbLife() {
		return nbLife;
	}

	public void setNbLife(int nbLife) {
		this.nbLife = nbLife;
	}

	public Weapon getNextMissile() {
		return weaponMissile.removeFirst();

	}

	public LinkedList<Weapon> getWeaponMissile() {
		return weaponMissile;
	}

	
	public int getNbMissile(){
		return weaponMissile.size();
	}

	public boolean hasBonus(){
		return bonus!=-1;
	}
	
	public int getBonus(){
		return bonus;
	}
	
	public Vec2 getPos(){
		return new Vec2(getBody().getPosition().x,getBody().getPosition().y);
	}

	public boolean remove() {
		if(getBody().getPosition().x<0||getBody().getPosition().x>Set.getSet().getWidth()){
			setAlive(false);
			return true;
		}
		if(getBody().getPosition().y<0||getBody().getPosition().y>Set.getSet().getHeight()){
			setAlive(false);
			return true;
		}
		return false;
	}
	
	
	public abstract void draw(Graphics2D graphics);

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}
}
