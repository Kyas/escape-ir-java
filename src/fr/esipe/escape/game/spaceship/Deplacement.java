package fr.esipe.escape.game.spaceship;

import org.jbox2d.common.Vec2;

import fr.esipe.escape.gesture.Gesture;

public class Deplacement {

	private final Gesture gesture_enCour;
	private final Vec2 posInitial;
	private final Vec2 posFinal;
	private final Vec2 force;
	
	
	
	public Deplacement(Gesture gesture_enCour, Vec2 posInitial, Vec2 posFinal,
			Vec2 force) {
		this.gesture_enCour = gesture_enCour;
		this.posInitial = posInitial;
		this.posFinal = posFinal;
		this.force = force;
	}


	public Vec2 getPosInitial() {
		return posInitial;
	}


	public Vec2 getPosFinal() {
		return posFinal;
	}


	public Vec2 getForce() {
		return force;
	}
	
	
	
	public Gesture getGesture_enCour() {
		return gesture_enCour;
	}


	public boolean finish_Gesture(Vec2 initposition,Vec2 position){
		return gesture_enCour.completeGesture(this,initposition,position);
	}
	
}
