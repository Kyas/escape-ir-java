package fr.esipe.escape.game.spaceship;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.Objects;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.Set;
import fr.esipe.escape.collisions.Mask;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.Map;
import fr.esipe.escape.game.bonus.Bonus;
import fr.esipe.escape.game.weapon.Munitions;
import fr.esipe.escape.gesture.Gesture.NameGesture;
import fr.esipe.escape.ui.drawable.ImageFactory;


public class Player extends SpaceShip implements BehaviourShip{


	private Deplacement movement;
	private Vec2 stronger;
	private static int life;
	private PolygonShape shapeDef;
	private static int WIDTH=47;
	private static int HEIGHT=62;
	private Vec2 lastPosition;
	private Vec2 initPosition;
	private int compteur1=1;
	private boolean isAlive;
	private boolean hasShield;

	public Player(int x,int y,int life) {
		super(x,y,ImageFactory.getImage("player.png"));
		Player.life = life;
		setAlive(true);
		getBodyDef().position.set(new Vec2(x, y));
		stronger=new Vec2(0,0);
		shapeDef = new PolygonShape();
		shapeDef.setAsBox(WIDTH/4, HEIGHT/4);
		getBodyDef().userData = this;
		getFixtureDef().shape = shapeDef;
		getFixtureDef().userData = EntityType.PLAYER;
		getFixtureDef().filter.categoryBits = Mask.CATEGORY_PLAYER;
		getFixtureDef().filter.maskBits = Mask.MASK_PLAYER;
		getFixtureDef().density = 0.f;
		getFixtureDef().friction = 0.1f;
		getFixtureDef().restitution = 0.1f;
		setBody(Map.getWorld().createBody(getBodyDef()));
		getBody().createFixture(getFixtureDef());
		lastPosition=new Vec2(getBody().getPosition().x,getBody().getPosition().y);
	}


	private void update(){
		if(movement!=null){
			if(movement.getGesture_enCour().getName()==NameGesture.LEFTLOOPING){
				getBody().setActive(false);
				switch (compteur1) {
				case 1:	setImg(ImageFactory.getImage("rotation/enemy-simple15.png"));break;
				case 2:	setImg(ImageFactory.getImage("rotation/enemy-simple14.png"));break;
				case 3:	setImg(ImageFactory.getImage("rotation/enemy-simple13.png"));break;
				case 4:	setImg(ImageFactory.getImage("rotation/enemy-simple12.png"));break;
				case 5:	setImg(ImageFactory.getImage("rotation/enemy-simple11.png"));break;
				case 6:	setImg(ImageFactory.getImage("rotation/enemy-simple10.png"));break;
				case 7:	setImg(ImageFactory.getImage("rotation/enemy-simple9.png"));break;
				case 8:	setImg(ImageFactory.getImage("rotation/enemy-simple8.png"));break;
				case 9:	setImg(ImageFactory.getImage("rotation/enemy-simple7.png"));break;
				case 10:setImg(ImageFactory.getImage("rotation/enemy-simple6.png"));break;
				case 11:setImg(ImageFactory.getImage("rotation/enemy-simple5.png"));break;
				case 12:setImg(ImageFactory.getImage("rotation/enemy-simple4.png"));break;
				case 13:setImg(ImageFactory.getImage("rotation/enemy-simple3.png"));break;
				case 14:setImg(ImageFactory.getImage("rotation/enemy-simple2.png"));break;
				case 15:setImg(ImageFactory.getImage("rotation/enemy-simple1.png"));break;
				case 16:
				{movement=null;
				compteur1=1;
				setImg(ImageFactory.getImage("player.png"));
				getBody().setActive(true);
				break;
				}
				default:break;
				}
				compteur1++;
			}
			else if(movement.getGesture_enCour().getName()==NameGesture.RIGTH_LOOPING){

				getBody().setActive(false);
				switch (compteur1) {
				case 1:	setImg(ImageFactory.getImage("rotation/enemy-simple1.png"));break;
				case 2:	setImg(ImageFactory.getImage("rotation/enemy-simple2.png"));break;
				case 3:	setImg(ImageFactory.getImage("rotation/enemy-simple3.png"));break;
				case 4:	setImg(ImageFactory.getImage("rotation/enemy-simple4.png"));break;
				case 5:	setImg(ImageFactory.getImage("rotation/enemy-simple5.png"));break;
				case 6:	setImg(ImageFactory.getImage("rotation/enemy-simple6.png"));break;
				case 7:	setImg(ImageFactory.getImage("rotation/enemy-simple7.png"));break;
				case 8:	setImg(ImageFactory.getImage("rotation/enemy-simple8.png"));break;
				case 9:	setImg(ImageFactory.getImage("rotation/enemy-simple9.png"));break;
				case 10:setImg(ImageFactory.getImage("rotation/enemy-simple10.png"));break;
				case 11:setImg(ImageFactory.getImage("rotation/enemy-simple11.png"));break;
				case 12:setImg(ImageFactory.getImage("rotation/enemy-simple12.png"));break;
				case 13:setImg(ImageFactory.getImage("rotation/enemy-simple13.png"));break;
				case 14:setImg(ImageFactory.getImage("rotation/enemy-simple14.png"));break;
				case 15:setImg(ImageFactory.getImage("rotation/enemy-simple15.png"));break;
				case 16:
				{movement=null;
				compteur1=1;
				setImg(ImageFactory.getImage("player.png"));
				getBody().setActive(true);
				break;
				}
				default:break;

				}

				compteur1++;
			}
			else {
				if(blockPosition()||movement.finish_Gesture(initPosition,new Vec2(getBody().getPosition().x,getBody().getPosition().y))){
					movement=null;
					stronger=new Vec2(0,0);
					if(blockPosition()){
						reinit(lastPosition);
					}
				}

				if(!blockPosition()){
					lastPosition=new Vec2(getBody().getPosition().x,getBody().getPosition().y);
				}
			}
		}
		else{
			initPosition=new Vec2(getBody().getPosition());
		}
	}

	@Override
	public void move(int x, int y) {
		Vec2 force = new Vec2(x, y);
		getBody().setLinearVelocity(force);
	}


	public void draw(Graphics2D graphics) {
		update();
		SpaceShip.resetTrans(graphics);
		graphics.translate(getBody().getPosition().x, getBody().getPosition().y);
		graphics.setColor(Color.WHITE);
		graphics.drawImage(getImg(),WIDTH/2, -HEIGHT/2, WIDTH, HEIGHT, null);
		move((int)stronger.x,(int)stronger.y);
	}

	public static int getLife() {
		return life;
	}

	public static void setLife(int life) {
		Player.life = life;
	}

	public void setMovement(Deplacement movement) {
		this.movement = movement;
	}

	public void takeBonus(Bonus bonus) {
		Objects.requireNonNull(bonus);
		EntityType t = (EntityType) bonus.getFixtureDef().userData;
		switch(t) {
		case AMMO:
			Map.getWeaponsTable().put("Laser", new Munitions("Laser", 2));
			break;
		case HEALTH:
			int life = Player.getLife() + 10;
			Player.setLife(life);
			break;
		case SHIELD:
			this.setHasShield(true);
			break;
		}
	}

	public void setVec2(Vec2 stronger){
		this.stronger=stronger;
	}

	private boolean blockPosition(){
		int x=(int) getBody().getPosition().x;
		int y=(int) getBody().getPosition().y;
		Boolean retour=false;
		if(x<=10 || x>= Set.getSet().getWidth()-getElement().getWidth()-10) retour=true;
		if(y<(Set.getSet().getHeight()*2)/3|| y>= Set.getSet().getHeight()-getElement().getHeight()-10) retour= true;
		return retour;
	}


	public boolean isAlive() {
		return isAlive;
	}


	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public boolean isHasShield() {
		return hasShield;
	}


	public void setHasShield(boolean hasShield) {
		this.hasShield = hasShield;
	}
}
