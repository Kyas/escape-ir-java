package fr.esipe.escape.game.spaceship.boss;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.LinkedList;
import java.util.Random;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.Set;
import fr.esipe.escape.collisions.Mask;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.Map;
import fr.esipe.escape.game.spaceship.SpaceShip;
import fr.esipe.escape.game.weapon.Fireball;
import fr.esipe.escape.game.weapon.Missile;
import fr.esipe.escape.game.weapon.Shiboleet;
import fr.esipe.escape.game.weapon.Weapon;
import fr.esipe.escape.ui.drawable.ImageFactory;

public class Boss3 extends AbstractBoss {

	private long frequence = 3000;
	private long startTime = 0;
	private PolygonShape shapeDef;
	private Vec2 stronger;
	private static int WIDTH = 168;
	private static int HEIGHT = 135;

	public Boss3(int x, int y) {
		super(x, y, ImageFactory.getImage("orbital-boss3.png"),
				new LinkedList<Weapon>(), 10, 2.5);
		shapeDef = new PolygonShape();
		shapeDef.setAsBox(WIDTH / 3, HEIGHT / 3);
		getBodyDef().position = new Vec2(x, y);
		getBodyDef().userData = this;
		getFixtureDef().shape = shapeDef;
		getFixtureDef().density = 0.f;
		getFixtureDef().friction = 0.1f;
		getFixtureDef().restitution = 0.1f;
		getFixtureDef().isSensor = true;
		getFixtureDef().filter.categoryBits = Mask.CATEGORY_ENEMIES;
		getFixtureDef().filter.maskBits = Mask.MASK_ENEMIES;
		getFixtureDef().userData = EntityType.BOSS;
		setBody(Map.getWorld().createBody(getBodyDef()));
		getBody().createFixture(getFixtureDef());
		stronger = new Vec2(-50, 0);
	}

	public void update() {
		long actualTime = System.currentTimeMillis();
		if (actualTime - startTime > frequence) {
			Vec2 force;
			if (getBody().getPosition().x > Set.getSet().getWidth() / 2) {
				force = new Vec2(-20, 50);
			} else {
				force = new Vec2(20, 50);
			}
			int type_arme = Math.abs(new Random().nextInt() % 3);
			if (type_arme == 0) {
				getWeaponMissile().add(
						new Missile(new Vec2(getBody().getPosition().x + 23,
								getBody().getPosition().y + 62), force, false));
			} else if (type_arme == 1) {
				getWeaponMissile().add(
						new Fireball(new Vec2(getBody().getPosition().x + 23,
								getBody().getPosition().y + 62), force, false));
			} else {
				getWeaponMissile().add(
						new Shiboleet(new Vec2(getBody().getPosition().x + 23,
								getBody().getPosition().y + 62), force, false));
			}
			getWeaponMissile().add(
					new Fireball(new Vec2(
							getBody().getPosition().x + WIDTH / 2, getBody()
									.getPosition().y + HEIGHT / 2), force,
							false));
			startTime = actualTime;
		}

		if (getBody().getPosition().x < 20
				&& getBody().getPosition().y <= Set.getSet().getHeight() / 4) {
			stronger = new Vec2(0, 50);
		} else if (getBody().getPosition().x > Set.getSet().getWidth() - WIDTH
				- 50
				&& getBody().getPosition().y <= Set.getSet().getHeight() / 4) {
			stronger = new Vec2(-50, 0);
		} else if (getBody().getPosition().x > Set.getSet().getWidth() - WIDTH
				- 50
				&& getBody().getPosition().y > Set.getSet().getHeight() / 2) {
			stronger = new Vec2(0, -50);
		} else if (getBody().getPosition().x < 20
				&& getBody().getPosition().y > Set.getSet().getHeight() / 2) {
			stronger = new Vec2(50, 0);
		}

	}

	@Override
	public void move(int x, int y) {
		getBody().setLinearVelocity(new Vec2(x, y));
	}

	public void draw(Graphics2D graphics) {
		update();
		SpaceShip.resetTrans(graphics);
		graphics.translate(getBody().getPosition().x, getBody().getPosition().y);
		graphics.setColor(Color.WHITE);
		graphics.drawImage(getImg(), WIDTH / 2, -HEIGHT / 2, WIDTH, HEIGHT,
				null);
		move((int) stronger.x, (int) stronger.y);
	}

	@Override
	public boolean isDead() {
		return getNbLife() == 0;
	}

}
