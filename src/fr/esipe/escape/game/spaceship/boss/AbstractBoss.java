package fr.esipe.escape.game.spaceship.boss;

import java.awt.Image;
import java.util.LinkedList;

import fr.esipe.escape.game.spaceship.enemy.SpaceShipEnemy;
import fr.esipe.escape.game.weapon.Weapon;

public abstract class AbstractBoss extends SpaceShipEnemy {

	public AbstractBoss(int x, int y, Image img, LinkedList<Weapon> list_w,
			int nbLive, double frequence) {
		super(x, y, img, list_w, nbLive);
		// TODO Auto-generated constructor stub
	}
	
	public abstract boolean isDead();
}
