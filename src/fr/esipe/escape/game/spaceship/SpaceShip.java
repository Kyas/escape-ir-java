package fr.esipe.escape.game.spaceship;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.util.Objects;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

public abstract class SpaceShip {

	private final Taille element;
	private  Image img;

	private BodyDef bodyDef;
	private FixtureDef fixtureDef;
	private Body body;

	public SpaceShip(int x, int y, Image img) {
		this.element = new Taille(img.getHeight(null), img.getWidth(null));
		this.img = img;
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.linearDamping = .5f;
		bodyDef.userData = this;
		fixtureDef = new FixtureDef();
		fixtureDef.isSensor = true;
	}


	
	public void reinit(Vec2 lastPosition){
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.position = lastPosition;
		bodyDef.linearDamping = .5f;
		bodyDef.userData = this;
		fixtureDef = new FixtureDef();
		fixtureDef.isSensor = true;
	}

	
	public Taille getElement() {
		return element;
	}

	public Image getImg() {
		return img;
	}
	public void setImg(Image img) {
		this.img = img;
	}


	public static void resetTrans(Graphics2D graphics) {
		AffineTransform at = new AffineTransform();
		at.setToIdentity();
		graphics.setTransform(at);
	}

	/**
	 * @return body, the body of the element
	 */
	public Body getBody() {
		return this.body;
	}

	public void setBody(Body body) {
		this.body = Objects.requireNonNull(body);
		// Setup volume friction
		this.body.setLinearDamping(.5f);
	}

	/**
	 * @return bodyDef, the bodyDef of the element
	 */
	public BodyDef getBodyDef() {
		return this.bodyDef;
	}

	/**
	 * @return fixtureDef, the fixtureDef of the element
	 */
	public FixtureDef getFixtureDef() {
		return this.fixtureDef;
	}

}
