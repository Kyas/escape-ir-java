package fr.esipe.escape.game;

/**
 * This enum class is used to input an enum in our fixture definition of our
 * bodies. It is mainly used for collisions to see in details which EntityType
 * we are looking for.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public enum EntityType {

	// World Type.
	WALL("Wall"),

	// SpaceshipPlayer.
	PLAYER("Player"),

	// Spaceshipenemies
	ENEMIES("Enemies"), BOSS("Boss"),

	// Weapons From Player
	MISSILEPLAYER("MissilePlayer"), FIREBALLPLAYER("FireballPlayer"), SHIBOLEETPLAYER(
			"ShiboleetPlayer"), LASERPLAYER("LaserPlayer"),

	// Weapons From Enemy
	WEAPONENEMY("WeaponEnemy"),

	// Bonus Type.
	AMMO("Ammo"), HEALTH("Health"), SHIELD("Shield");

	private String type;

	EntityType(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}
}
