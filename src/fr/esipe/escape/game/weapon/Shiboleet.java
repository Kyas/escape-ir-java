package fr.esipe.escape.game.weapon;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.LinkedList;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.collisions.Mask;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.Map;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * This class implements the weapon Shiboleet.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class Shiboleet extends Weapon {

	private static LinkedList<LittleShiboleet> listOfShibos;

	private CircleShape shapeDef;
	private float scalingRadius;
	private boolean displayLittleShibos;
	private boolean isPlayer;

	private Image img = ImageFactory.getImage("shiboleet.png");

	public Shiboleet(Vec2 vec, Vec2 force, boolean isPlayer) {
		super(vec, force, isPlayer);

		this.isPlayer = isPlayer;
		shapeDef = new CircleShape();
		scalingRadius = 0;
		shapeDef.m_radius = scalingRadius;

		getBodyDef().userData = this;

		getFixtureDef().shape = shapeDef;
		if (isPlayer) {
			getFixtureDef().userData = EntityType.SHIBOLEETPLAYER;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			getFixtureDef().userData = EntityType.WEAPONENEMY;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}
		getFixtureDef().density = 0.f;
		getFixtureDef().friction = 0.1f;
		getFixtureDef().restitution = 0.5f;

		getBody().createFixture(getFixtureDef());

		listOfShibos = new LinkedList<LittleShiboleet>();

		for (int i = 0; i < 5; i++) {
			LittleShiboleet ls = new LittleShiboleet(new Vec2(getBody()
					.getPosition().x, getBody().getPosition().y), i,
					getStronger(), isPlayer);
			listOfShibos.add(ls);
		}

	}

	@Override
	public void move(Vec2 stronger) {
		if (scalingRadius < 80) {
			// Nothing to do.
		} else {
			// this.moveLittleShibos();
			if (!displayLittleShibos) {
				addLittleShibos();
			}
		}

	}

	public void addLittleShibos() {
		for (LittleShiboleet ls : listOfShibos) {
			Map.getWeaponsList().add(ls);
		}
		displayLittleShibos = true;
		Map.remove(this);
	}

	public void moveLittleShibos() {
		if (!listOfShibos.isEmpty() && listOfShibos.size() == 5) {
			// Parcourir la liste des littleshiboleet et appeller la m�thode
			// move.
			// Apply the move.
			listOfShibos.get(0).move(-50, 0);
			listOfShibos.get(1).move(-50, -50);
			listOfShibos.get(2).move(0, -50);
			listOfShibos.get(3).move(50, 0);
			listOfShibos.get(4).move(50, -50);

		}

	}

	public void removeLittleShibos() {
		if (!listOfShibos.isEmpty()) {
			listOfShibos.remove();
		}
	}

	@Override
	public void draw(Graphics2D graphics) {
		// Reset the graphic in the world.
		Weapon.resetTrans(graphics);
		graphics.translate(getBody().getPosition().x, getBody().getPosition().y);
		graphics.setColor(Color.WHITE);
		if (scalingRadius < 80) {
			scalingRadius += 0.50;
		}
		// Update of the radius.
		shapeDef.m_radius = scalingRadius;
		if (!displayLittleShibos) {
			// Remove the previous image and draw the final one above.
			graphics.drawImage(img, (int) -scalingRadius / 2 + 20,
					(int) -scalingRadius / 2 + 15, (int) scalingRadius,
					(int) scalingRadius, null);
		}
		this.move(getStronger());
	}

	@Override
	public EntityType getType() {
		if (isPlayer)
			return EntityType.SHIBOLEETPLAYER;
		return EntityType.WEAPONENEMY;
	}

}
