package fr.esipe.escape.game.weapon;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.collisions.Mask;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * This class implements the weapon LittleShiboleet.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class LittleShiboleet extends Weapon {

	private CircleShape shapeDef;
	private int id;
	private boolean isPlayer;

	private Image img = ImageFactory.getImage("shiboleet.png");
	private static int WIDTH = 30;
	private static int HEIGHT = 30;

	public LittleShiboleet(Vec2 vec,int id, Vec2 force, boolean isPlayer) {
		super(vec, force, isPlayer);
		this.id = id;

		shapeDef = new CircleShape();
		shapeDef.m_radius = WIDTH / 2;

		getBodyDef().userData = this;

		getFixtureDef().shape = shapeDef;
		if(isPlayer) {
			getFixtureDef().userData = EntityType.SHIBOLEETPLAYER;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			getFixtureDef().userData = EntityType.WEAPONENEMY;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}
		getFixtureDef().density = 0.f;
		getFixtureDef().friction = 0.1f;
		getFixtureDef().restitution = 0.5f;

		getBody().createFixture(getFixtureDef());
	}

	@Override
	public void move(Vec2 stronger) {
		// Nothing to do.
		if(isPlayer()==true){
			switch (id) {
			case 0:
				move(50, -50);
				break;
			case 1:
				move(-50, -50);
				break;
			case 2:
				move(0, -75);
				break;
			case 3:
				move(-50, -100);
				break;
			case 4:
				move(50, -100);
				break;
			}
		}else{
			switch (id) {
			case 0:
				move(-50, 50);
				break;
			case 1:
				move(50, 50);
				break;
			case 2:
				move(0, 75);
				break;
			case 3:
				move(50, 100);
				break;
			case 4:
				move(-50, 100);
				break;
			}
		}
	}

	public void move(int x, int y) {
		Vec2 force = new Vec2(x, y);
		Vec2 point = getBody().getWorldPoint(getBody().getPosition());
		getBody().applyLinearImpulse(force, point);
	}

	@Override
	public void draw(Graphics2D graphics) {
		Weapon.resetTrans(graphics);
		graphics.translate(getBody().getPosition().x, getBody().getPosition().y);
		graphics.setColor(Color.WHITE);
		graphics.drawImage(img, -WIDTH / 2 + 20, -HEIGHT / 2 + 15, WIDTH,
				HEIGHT, null);
		this.move(getStronger());
	}

	@Override
	public EntityType getType() {
		if (isPlayer)
			return EntityType.SHIBOLEETPLAYER;
		return EntityType.WEAPONENEMY;
	}

}
