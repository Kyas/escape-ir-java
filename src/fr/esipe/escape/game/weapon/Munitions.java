package fr.esipe.escape.game.weapon;

/**
 * This class implements munitions of each Weapon.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class Munitions {

	private final String weapon;
	private int nbAmmos;
	private boolean isEmpty;

	public Munitions(String weapon, int nbAmmos) {
		this.weapon = weapon;
		this.setNbAmmos(nbAmmos);
	}

	public void remove() {
		if (nbAmmos > 1) {
			nbAmmos--;
		} else {
			nbAmmos = 0;
			isEmpty = true;
		}
	}

	public boolean isEmpty() {
		return isEmpty;
	}

	public String getWeapon() {
		return weapon;
	}

	public int getNbAmmos() {
		return nbAmmos;
	}

	public void setNbAmmos(int nbAmmos) {
		if (nbAmmos <= 0) {
			throw new IllegalArgumentException();
		} else {
			this.nbAmmos = nbAmmos;
		}
	}

}
