package fr.esipe.escape.game.weapon;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.collisions.Mask;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * This class implements the weapon Laser.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class Laser extends Weapon {

	private PolygonShape shapeDef;

	private Image img = ImageFactory.getImage("laser-rouge.png");
	private static int WIDTH = 70;
	private static int HEIGHT = 330;

	public Laser(Vec2 vec, Vec2 force ,boolean isPlayer) {
		super(vec, force, isPlayer);

		shapeDef = new PolygonShape();
		shapeDef.setAsBox(WIDTH / 2, HEIGHT / 2);

		getBodyDef().userData = this;

		getFixtureDef().shape = shapeDef;
		if(isPlayer) {
			getFixtureDef().userData = EntityType.LASERPLAYER;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			getFixtureDef().userData = EntityType.WEAPONENEMY;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}
		getFixtureDef().density = 0.f;
		getFixtureDef().friction = 0.2f;
		getFixtureDef().restitution = 0.f;

		getBody().createFixture(getFixtureDef());
	}


	@Override
	public void draw(Graphics2D graphics) {
		Weapon.resetTrans(graphics);
		graphics.translate(getBody().getPosition().x, getBody().getPosition().y);
		graphics.setColor(Color.WHITE);
		graphics.drawImage(img, -WIDTH / 2 + 20, -HEIGHT + 25, WIDTH, HEIGHT,
				null);
		move(new Vec2(0, 0));
	}

	@Override
	public EntityType getType() {
		return EntityType.LASERPLAYER;
	}


	@Override
	public void move(Vec2 stronger) {
		Vec2 force = new Vec2(0, -1);
		Vec2 point = getBody().getWorldPoint(getBody().getPosition());
		getBody().applyLinearImpulse(force, point);
		
	}


}
