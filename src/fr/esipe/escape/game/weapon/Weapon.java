package fr.esipe.escape.game.weapon;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.Objects;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.Map;
import fr.esipe.escape.game.Movable;

/**
 * This abstract class implements Weapons in general (Missile, Fireball,
 * Shiboleet etc.). It implements also the interface Movable to let weapons to
 * move during the game.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public abstract class Weapon implements Movable {

	private BodyDef bodyDef;
	private FixtureDef fixtureDef;
	private Body body;
	private final Vec2 stronger;
	
	private boolean alive;
	private boolean isPlayer;


	public Weapon(Vec2 vec,Vec2 force, boolean isPlayer) {
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.position = Objects.requireNonNull(vec);
		bodyDef.linearDamping = .5f;
		bodyDef.userData = this;
		fixtureDef = new FixtureDef();
		fixtureDef.isSensor = true;

		this.setPlayer(isPlayer);
		
		alive = true;

		body = Map.getWorld().createBody(bodyDef);
		stronger=force;
	}

	@Override
	public abstract void move(Vec2 force);

	public abstract EntityType getType();

	/**
	 * Necessary to update positions of a weapon according to the position of
	 * the spaceship player in the world.
	 * 
	 * @param vec
	 *            The vector.
	 */
	public void updatePos(Vec2 vec) {

	}

	public static void resetTrans(Graphics2D graphics) {
		AffineTransform at = new AffineTransform();
		at.setToIdentity();
		graphics.setTransform(at);
	}

	/**
	 * At refresh time, draw method of all the elements of the world are called
	 * in order to draw their graphic representation
	 * 
	 * @param graphics
	 *            to draw
	 */
	public abstract void draw(Graphics2D graphics);

	/**
	 * When you call addElement in World class, it set the body reference in
	 * this class. Also set the damping of all element
	 * 
	 * @param body
	 *            reference
	 */
	public void setBody(Body body) {
		this.body = Objects.requireNonNull(body);
		// Setup volume friction
		this.body.setLinearDamping(.5f);
	}

	/**
	 * @return body, the body of the element
	 */
	public Body getBody() {
		return this.body;
	}

	/**
	 * @return bodyDef, the bodyDef of the element
	 */
	public BodyDef getBodyDef() {
		return this.bodyDef;
	}

	/**
	 * @return fixtureDef, the fixtureDef of the element
	 */
	public FixtureDef getFixtureDef() {
		return this.fixtureDef;
	}

	/**
	 * Remove the weapon in the world.
	 */
	public void remove() {
		Map.remove(this);
	}


	public Vec2 getStronger() {
		return stronger;
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public boolean isPlayer() {
		return isPlayer;
	}

	public void setPlayer(boolean isPlayer) {
		this.isPlayer = isPlayer;
	}
}
