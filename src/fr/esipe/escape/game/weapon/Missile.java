package fr.esipe.escape.game.weapon;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.collisions.Mask;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * This class implements the weapon Missile.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class Missile extends Weapon {

	private PolygonShape shapeDef;
	private boolean isPlayer;

	private Image img = ImageFactory.getImage("missile.png");
	private static int WIDTH = 7;
	private static int HEIGHT = 12;

	public Missile(Vec2 vec, Vec2 force, boolean isPlayer) {
		super(vec, force, isPlayer);
		this.isPlayer = isPlayer;
		
		shapeDef = new PolygonShape();
		shapeDef.setAsBox(WIDTH/2, HEIGHT/2);
		
		getBodyDef().userData = this;
		
		getFixtureDef().shape = shapeDef;
		if(isPlayer) {
			getFixtureDef().userData = EntityType.MISSILEPLAYER;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			getFixtureDef().userData = EntityType.WEAPONENEMY;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}
		getFixtureDef().density = 0.f;
		getFixtureDef().friction = 0.1f;
		getFixtureDef().restitution = 0.1f;
		
		getBody().createFixture(getFixtureDef());
	}

	@Override
	public void move(Vec2 stronger) {
		Vec2 force = new Vec2(stronger.x, stronger.y);
		Vec2 point = getBody().getWorldPoint(getBody().getWorldCenter());
		getBody().applyLinearImpulse(force, point);
	}
	
	@Override
	public void draw(Graphics2D graphics) {
		Weapon.resetTrans(graphics);
		graphics.translate(getBody().getPosition().x, getBody().getPosition().y);
		graphics.setColor(Color.WHITE);
		graphics.drawImage(img, WIDTH/2 + 13, -HEIGHT/2, WIDTH, HEIGHT, null);
		this.move(getStronger());
	}

	@Override
	public EntityType getType() {
		if (isPlayer)
			return EntityType.MISSILEPLAYER;
		return EntityType.WEAPONENEMY;
	}

}
