package fr.esipe.escape.game.weapon;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.collisions.Mask;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * This class implements the weapon Fireball.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class Fireball extends Weapon {

	private CircleShape shapeDef;
	private float scalingRadius;
	private boolean isPlayer;

	private Image img = ImageFactory.getImage("fireball.png");
	private static int SIZE = 60;

	public Fireball(Vec2 vec, Vec2 force, boolean isPlayer) {
		super(vec, force, isPlayer);
		this.isPlayer = isPlayer;
		shapeDef = new CircleShape();
		scalingRadius = 0;
		shapeDef.m_radius = SIZE / 2;

		getBodyDef().userData = this;

		getFixtureDef().shape = shapeDef;
		if (isPlayer) {
			getFixtureDef().userData = EntityType.FIREBALLPLAYER;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_PLAYER;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_PLAYER;
		} else {
			getFixtureDef().userData = EntityType.WEAPONENEMY;
			getFixtureDef().filter.categoryBits = Mask.CATEGORY_WEAPONS_ENEMIES;
			getFixtureDef().filter.maskBits = Mask.MASK_WEAPONS_ENEMIES;
		}
		getFixtureDef().density = 0.f;
		getFixtureDef().friction = 0.1f;
		getFixtureDef().restitution = 0.1f;

		getBody().createFixture(getFixtureDef());
	}

	@Override
	public void move(Vec2 force1) {
		if (scalingRadius < 60) {
			// Nothing to do.
		} else {
			// Apply the move.
			Vec2 force = new Vec2(force1.x, force1.y);
			Vec2 point = getBody().getWorldPoint(getBody().getWorldCenter());
			getBody().applyLinearImpulse(force, point);

		}
	}

	@Override
	public void draw(Graphics2D graphics) {
		// Reset the graphic in the world.
		Weapon.resetTrans(graphics);
		graphics.translate(getBody().getPosition().x, getBody().getPosition().y);
		graphics.setColor(Color.WHITE);
		if (scalingRadius < 60) {
			scalingRadius += 0.50;
		}
		// Update of the radius.
		shapeDef.m_radius = scalingRadius;
		// Remove the previous image and draw the final one above.
		graphics.drawImage(img, (int) -scalingRadius / 2 + 20,
				(int) -scalingRadius / 2 - 20, (int) scalingRadius,
				(int) scalingRadius, null);
		this.move(getStronger());
	}

	@Override
	public EntityType getType() {
		if (isPlayer)
			return EntityType.FIREBALLPLAYER;
		return EntityType.WEAPONENEMY;
	}

}
