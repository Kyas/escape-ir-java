package fr.esipe.escape.game.activeWeapon;

import java.util.List;

import org.jbox2d.common.Vec2;

import fr.esipe.escape.game.spaceship.SpaceShip;

/**
 * Used for the active weapon.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class WeaponActive {

	public static boolean detectGesture(Vec2 listVector, SpaceShip player) {
		int xMin = (int) player.getBody().getPosition().x;
		int xMax = xMin + 47;
		int yMin = (int) player.getBody().getPosition().y;
		int yMax = yMin + 62;
		Vec2 lVec = listVector;
		if (lVec.x < xMax - 47 || lVec.x > xMax + 47)
			return false;
		if (lVec.y < yMin - 62 || lVec.y > yMax + 47)
			return false;
		return true;
	}

	public static Vec2 calculPath(List<Vec2> listVector, SpaceShip player) {
		float firstX = listVector.get(0).x;
		float firstY = listVector.get(0).y;
		float lastX = listVector.get(listVector.size() - 1).x;
		float lastY = listVector.get(listVector.size() - 1).y;
		float forceX, forceY;
		if (firstX < lastX)
			forceX = 1 * ((lastX - firstX) * 60 / 100);
		else
			forceX = -1 * ((firstX - lastX) * 60 / 100);
		if (firstY < lastY)
			forceY = 1 * ((lastY - firstY) * 60 / 100);
		else
			forceY = -1 * ((firstY - lastY) * 60 / 100);
		return new Vec2(forceX, forceY);
	}

	public static void shoot() {

	}

}
