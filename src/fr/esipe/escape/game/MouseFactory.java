package fr.esipe.escape.game;

import fr.umlv.zen2.MotionEvent;

/**
 * Used for click event in the MenuActivity.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class MouseFactory {

	/**
	 * Event of the menu.
	 * 
	 * @param event
	 *            The event
	 * @return A number depending of the zone of a click.
	 */
	public static int eventOnMenuActivity(MotionEvent event) {
		int mouseX = event.getX(), mouseY = event.getY();
		if ((mouseX > 200 && mouseX < 430) && (mouseY > 280 && mouseY < 340)) {
			return 1;
		} else if ((mouseX > 200 && mouseX < 430)
				&& (mouseY > 365 && mouseY < 425)) {
			return 2;
		} else if ((mouseX > 200 && mouseX < 430)
				&& (mouseY > 465 && mouseY < 530)) {
			return 3;
		}
		return 0;
	}

	/**
	 * Event of the menu activity level.
	 * 
	 * @param event
	 *            The event
	 * @return A number depending of the zone of a click.
	 */
	public static int eventOnMenuActivityLevel(MotionEvent event) {
		int mouseX = event.getX(), mouseY = event.getY();
		if ((mouseX > 60 && mouseX < 265) && (mouseY > 165 && mouseY < 310)) {
			return 1;
		} else if ((mouseX > 320 && mouseX < 540)
				&& (mouseY > 250 && mouseY < 400)) {
			return 2;
		} else if ((mouseX > 110 && mouseX < 330)
				&& (mouseY > 405 && mouseY < 555)) {
			return 3;
		} else if ((mouseX > 410 && mouseX < 560)
				&& (mouseY > 515 && mouseY < 560)) {
			return 4;

		}
		return 0;
	}

	/**
	 * Event of the menu activity options.
	 * 
	 * @param event
	 *            The event
	 * @return A number depending of the zone of a click.
	 */
	public static boolean eventOnMenuActivityOptions(MotionEvent event) {
		int mouseX = event.getX(), mouseY = event.getY();
		if ((mouseX > 410 && mouseX < 560) && (mouseY > 515 && mouseY < 560)) {
			return true;
		}
		return false;
	}

	/**
	 * Event of the menu activity credits.
	 * 
	 * @param event
	 *            The event
	 * @return A number depending of the zone of a click.
	 */
	public static boolean eventOnMenuActivityCredits(MotionEvent event) {
		int mouseX = event.getX(), mouseY = event.getY();
		if ((mouseX > 190 && mouseX < 410) && (mouseY > 385 && mouseY < 440)) {
			return true;
		}
		return false;
	}

	/**
	 * Used for the weapon Bar.
	 * 
	 * @param event
	 *            The event.
	 * @return the weapon Active.
	 */
	public static int eventOnWeapon(MotionEvent event) {
		int actual = -1;
		int mouseY = event.getY();
		if (mouseY > 160 && mouseY < 204) {
			actual = 0;
		} else if (mouseY > 205 && mouseY <= 255) {
			actual = 1;
		} else if (mouseY > 255 && mouseY <= 295) {
			actual = 2;
		} else if (mouseY > 295 && mouseY < 350) {
			actual = 3;
		} else {
			// Error.
			actual = -1;
		}
		return actual;
	}

}
