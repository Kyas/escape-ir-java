package fr.esipe.escape.game.bonus;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.collisions.Mask;
import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.weapon.Weapon;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * Bonus for Health.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class Health extends Bonus {

	private PolygonShape shapeDef;
	private Vec2 stronger;
	private Image img = ImageFactory.getImage("health.png");
	private static int WIDTH = 32;
	private static int HEIGHT = 32;

	public Health(Vec2 vec, Vec2 force) {
		super(vec, force);

		shapeDef = new PolygonShape();
		shapeDef.setAsBox(WIDTH / 2, HEIGHT / 2);

		getBodyDef().userData = this;

		getFixtureDef().shape = shapeDef;
		getFixtureDef().userData = EntityType.HEALTH;
		getFixtureDef().filter.categoryBits = Mask.CATEGORY_BONUS;
		getFixtureDef().filter.maskBits = Mask.MASK_BONUS;
		getFixtureDef().density = 0.f;
		getFixtureDef().friction = 0.f;
		getFixtureDef().restitution = 0.f;

		getBody().createFixture(getFixtureDef());
	}

	@Override
	public void move(Vec2 stronger) {
		Vec2 force = new Vec2(0, 1);
		Vec2 point = getBody().getWorldPoint(getBody().getWorldCenter());
		getBody().applyLinearImpulse(force, point);
	}

	@Override
	public void draw(Graphics2D graphics) {
		Weapon.resetTrans(graphics);
		graphics.translate(getBody().getWorldCenter().x, getBody()
				.getWorldCenter().y);
		graphics.setColor(Color.WHITE);
		graphics.drawImage(img, -WIDTH / 2, -HEIGHT / 2, WIDTH, HEIGHT, null);
		this.move(stronger);
	}

	@Override
	public EntityType getType() {
		return EntityType.HEALTH;
	}

}
