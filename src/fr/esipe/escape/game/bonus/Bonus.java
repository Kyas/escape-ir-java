package fr.esipe.escape.game.bonus;

import java.awt.Graphics2D;
import java.util.Objects;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;

import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.Map;
import fr.esipe.escape.game.Movable;

/**
 * This class implements bonuses that the player can have during the game.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public abstract class Bonus implements Movable {

	private int x;
	private int y;

	private Graphics2D graphics;

	private BodyDef bodyDef;
	private FixtureDef fixtureDef;
	private Body body;

	private boolean alive;

	public Bonus(Vec2 vec, Vec2 force) {
		x = (int) vec.x;
		y = (int) vec.y;

		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DYNAMIC;
		bodyDef.position = Objects.requireNonNull(vec);
		bodyDef.linearDamping = .5f;
		bodyDef.userData = this;

		fixtureDef = new FixtureDef();
		fixtureDef.isSensor = true;
		alive = true;

		body = Map.getWorld().createBody(bodyDef);
	}

	/**
	 * At refresh time, draw method of all the elements of the world are called
	 * in order to draw their graphic representation
	 * 
	 * @param graphics
	 *            to draw
	 */
	public abstract void draw(Graphics2D graphics);

	public abstract EntityType getType();

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public Graphics2D getGraphics() {
		return graphics;
	}

	/**
	 * @return bodyDef, the bodyDef of the element
	 */
	public BodyDef getBodyDef() {
		return this.bodyDef;
	}

	public FixtureDef getFixtureDef() {
		return fixtureDef;
	}

	public Body getBody() {
		return body;
	}

	public void remove() {
		Map.remove(this);
	}

	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

}
