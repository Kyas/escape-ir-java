package fr.esipe.escape.collisions;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;

import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.Map;
import fr.esipe.escape.game.bonus.Bonus;
import fr.esipe.escape.game.spaceship.Player;
import fr.esipe.escape.game.spaceship.enemy.SpaceShipEnemy;
import fr.esipe.escape.game.weapon.Weapon;

/**
 * Manage all collisions in the World environment
 * 
 * @author C�line Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class Collision implements ContactListener {

	/**
	 * Manage the beginning of collisions of two fixtures.
	 */
	@Override
	public void beginContact(Contact contact) {

		// Object with fixture A.
		EntityType tA = (EntityType) contact.getFixtureA().getUserData();

		// Object with fixture B.
		EntityType tB = (EntityType) contact.getFixtureB().getUserData();

		int highscore = Map.getHIGHSCORE() + 500;

		/**
		 * Player <-> Enemies or WeaponEnemies or Bonus.
		 */
		if (tA == EntityType.PLAYER) {
			Player p = (Player) contact.getFixtureA().getBody().getUserData();
			int newLife = Player.getLife() - 1;
			switch (tB) {
			// Enemies
			case ENEMIES:
			case BOSS:
				SpaceShipEnemy spe = (SpaceShipEnemy) contact.getFixtureB()
						.getBody().getUserData();
				if (!p.isHasShield()) {
					int lifeEnemy = spe.getNbLife() - 1;
					spe.setNbLife(lifeEnemy);
					Player.setLife(newLife);
				} else {
					// The player has never the shield anymore.
					p.setHasShield(false);
				}
				Map.setHIGHSCORE(highscore);
				break;
			// WeaponEnemy
			case WEAPONENEMY:
				Weapon weaponEnemy = (Weapon) contact.getFixtureB().getBody()
						.getUserData();
				Player.setLife(newLife);
				weaponEnemy.remove();
				break;
			// Bonus
			case AMMO:
			case HEALTH:
			case SHIELD:
				Bonus b = (Bonus) contact.getFixtureB().getBody().getUserData();
				p.takeBonus(b);
				b.setAlive(false);
				Map.setHIGHSCORE(highscore);
				break;
			}
		}

		/**
		 * Enemies <-> Player or WeaponPlayer or Wall
		 */
		if (tA == EntityType.ENEMIES || tA == EntityType.BOSS) {
			SpaceShipEnemy spe = (SpaceShipEnemy) contact.getFixtureA()
					.getBody().getUserData();
			int newLife = spe.getNbLife() - 1;
			int newLifeP = Player.getLife() - 1;
			switch (tB) {
			// Player
			case PLAYER:
				Player p = (Player) contact.getFixtureB().getBody()
						.getUserData();
				if (!p.isHasShield()) {
					Player.setLife(newLifeP);
					spe.setNbLife(newLife);
				} else {
					p.setHasShield(false);
				}
				Map.setHIGHSCORE(highscore);
				break;
			// WeaponPlayer
			case MISSILEPLAYER:
			case FIREBALLPLAYER:
			case SHIBOLEETPLAYER:
				Weapon weaponPlayer = (Weapon) contact.getFixtureB().getBody()
						.getUserData();
				Map.setHIGHSCORE(highscore);
				weaponPlayer.setAlive(false);
				spe.setNbLife(newLife);
				Map.setHIGHSCORE(highscore);
				break;
			case LASERPLAYER:
				Map.setHIGHSCORE(highscore);
				spe.setNbLife(newLife);
				Map.setHIGHSCORE(highscore);
				break;
			// Wall
			case WALL:
				spe.setAlive(false);
				break;
			}
		}

		/**
		 * WeaponPlayer <-> Enemies or WeaponEnemy or Wall
		 */
		if (tA == EntityType.MISSILEPLAYER || tA == EntityType.FIREBALLPLAYER
				|| tA == EntityType.SHIBOLEETPLAYER
				|| tA == EntityType.LASERPLAYER) {
			Weapon weaponPlayer = (Weapon) contact.getFixtureA().getBody()
					.getUserData();
			switch (tB) {
			// Enemies
			case ENEMIES:
			case BOSS:
				SpaceShipEnemy spe = (SpaceShipEnemy) contact.getFixtureB()
						.getBody().getUserData();
				int lifeEnemy = spe.getNbLife() - 1;
				spe.setNbLife(lifeEnemy);
				weaponPlayer.setAlive(false);
				Map.setHIGHSCORE(highscore);
				break;
			// WeaponEnemy
			case WEAPONENEMY:
				Weapon weaponEnemy = (Weapon) contact.getFixtureB().getBody()
						.getUserData();
				weaponPlayer.setAlive(false);
				weaponEnemy.setAlive(false);
				Map.setHIGHSCORE(highscore);
				break;
			// Wall
			case WALL:
				weaponPlayer.setAlive(false);
				break;
			}
		}

		/**
		 * weaponEnemy <-> Player or WeaponPlayer or Wall
		 */
		if (tA == EntityType.WEAPONENEMY) {
			Weapon weaponEnemy = (Weapon) contact.getFixtureA().getBody()
					.getUserData();
			switch (tB) {
			// Player
			case PLAYER:
				int newLife = Player.getLife() - 1;
				Player.setLife(newLife);
				weaponEnemy.setAlive(false);
				break;
			// WeaponPlayer
			case MISSILEPLAYER:
			case FIREBALLPLAYER:
			case SHIBOLEETPLAYER:
				Weapon weaponPlayer = (Weapon) contact.getFixtureB().getBody()
						.getUserData();
				weaponPlayer.setAlive(false);
				weaponEnemy.setAlive(false);
				Map.setHIGHSCORE(highscore);
				break;
			case LASERPLAYER:
				weaponEnemy.setAlive(false);
				Map.setHIGHSCORE(highscore);
				break;
			// Wall
			case WALL:
				weaponEnemy.setAlive(false);
				break;
			}
		}

		/**
		 * Wall <-> Enemies or WeaponPlayer or WeaponEnemies or Bonus
		 */
		if (tA == EntityType.WALL) {
			switch (tB) {
			// Enemies
			case ENEMIES:
			case BOSS:
				SpaceShipEnemy spe = (SpaceShipEnemy) contact.getFixtureB()
						.getBody().getUserData();
				spe.setAlive(false);
				break;

			// Weapon
			case MISSILEPLAYER:
			case FIREBALLPLAYER:
			case SHIBOLEETPLAYER:
			case LASERPLAYER:
			case WEAPONENEMY:
				Weapon weaponEnemy = (Weapon) contact.getFixtureB().getBody()
						.getUserData();
				weaponEnemy.setAlive(false);
				break;
			// Bonus
			case AMMO:
			case HEALTH:
			case SHIELD:
				Bonus bonus = (Bonus) contact.getFixtureB().getBody()
						.getUserData();
				bonus.setAlive(false);
				break;
			}

			/**
			 * Wall <-> Enemies or WeaponPlayer or WeaponEnemies or Bonus
			 */
			if (tA == EntityType.AMMO || tA == EntityType.HEALTH
					|| tA == EntityType.SHIELD) {
				Bonus b = (Bonus) contact.getFixtureA().getBody().getUserData();
				switch (tB) {
				// Enemies
				case PLAYER:
					Player p = (Player) contact.getFixtureB().getBody()
							.getUserData();
					p.takeBonus(b);
					b.setAlive(false);
					break;

				// Wall
				case WALL:
					b.setAlive(false);
					break;
				}
			}
		}

	}

	/**
	 * Manage the end of collisions in a contact.
	 */
	@Override
	public void endContact(Contact contact) {

	}

	/**
	 * Manage the preSolve of collisions.
	 */
	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	/**
	 * Manage the postSolve of collisions.
	 */
	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

}
