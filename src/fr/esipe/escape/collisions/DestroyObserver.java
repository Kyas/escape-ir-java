package fr.esipe.escape.collisions;

import org.jbox2d.common.Vec2;

import fr.esipe.escape.game.EntityType;
import fr.esipe.escape.game.Map;
import fr.esipe.escape.game.bonus.Ammo;
import fr.esipe.escape.game.bonus.Bonus;
import fr.esipe.escape.game.bonus.Health;
import fr.esipe.escape.game.bonus.Shield;
import fr.esipe.escape.game.spaceship.Player;
import fr.esipe.escape.game.spaceship.enemy.SpaceShipEnemy;
import fr.esipe.escape.game.weapon.Weapon;

/**
 * This is a design pattern as an Observer where it is used to be a monitor. Its
 * task is to check if a body is dead in the World and we desactive / don't
 * display them in the future.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class DestroyObserver {

	/**
	 * Monitors bodies in the world if they are still alive.
	 */
	public static void update() {

		// Remove du spaceship.
		if (Map.getSpaceshipPlayer().isAlive()) {
			if (Player.getLife() <= 0) {
				Map.getSpaceshipPlayer().setAlive(false);
				Map.setLOSE(true);
			}
		}

		// Remove des ennemies.
		for (SpaceShipEnemy spe : Map.getSpaceshipEnemyList()) {
			if (spe.getNbLife() <= 0) {
				if (spe.hasBonus()) {
					int bonus = spe.getBonus();
					Bonus my_bonus;
					if (bonus == 0) {
						my_bonus = new Ammo(spe.getPos(), new Vec2(0, 30));
					} else if (bonus == 1) {
						my_bonus = new Health(spe.getPos(), new Vec2(0, 30));
					} else {
						my_bonus = new Shield(spe.getPos(), new Vec2(0, 30));
					}
					Map.getBonusList().add(my_bonus);
				}
				spe.setAlive(false);
				Map.remove(spe);
			}
		}

		for (Weapon p : Map.getWeaponsList()) {
			if (p.getFixtureDef().userData != EntityType.LASERPLAYER) {
				if (!p.isAlive()) {
					Map.remove(p);
				}
			} else {
				if (p.getBody().getPosition().y < -100) {
					Map.remove(p);
				}
			}
		}

		for (Bonus b : Map.getBonusList()) {
			if (!b.isAlive()) {
				Map.remove(b);
			}
		}
	}

}
