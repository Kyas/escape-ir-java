package fr.esipe.escape.collisions;

/**
 * This class is used for collisions, we put mask to make categories of some
 * objects between them if they should / should not collide during the game.
 * 
 * @author Celine Perillous <cperillous@etudiant.univ-mlv.fr>
 * @author Jeremy Lor <jlor@etudiant.univ-mlv.fr>
 * 
 */
public class Mask {

	/**
	 * The category of the player. 0000000000000001 in binary
	 */
	public static final short CATEGORY_PLAYER = 0x0001;

	/**
	 * The category of enemies. 0000000000000010 in binary
	 */
	public static final short CATEGORY_ENEMIES = 0x0002;

	/**
	 * The category of weapons of the player. 0000000000000100 in binary
	 */
	public static final short CATEGORY_WEAPONS_PLAYER = 0x0004;

	/**
	 * The category of weapons of enemies. 0000000000001000 in binary
	 */
	public static final short CATEGORY_WEAPONS_ENEMIES = 0x0008;

	/**
	 * The category of wall. 0000000000010000 in binary
	 */
	public static final short CATEGORY_WALL = 0x00016;

	/**
	 * The category of bonus. 0000000000100000 in binary
	 */
	public static final short CATEGORY_BONUS = 0x00032;

	/**
	 * The mask for the player.
	 */
	public static final short MASK_PLAYER = CATEGORY_ENEMIES
			| CATEGORY_WEAPONS_ENEMIES | CATEGORY_BONUS;

	/**
	 * The mask for enemies.
	 */
	public static final short MASK_ENEMIES = CATEGORY_PLAYER
			| CATEGORY_WEAPONS_PLAYER | CATEGORY_WALL;

	/**
	 * The mask of weapons of the player.
	 */
	public static final short MASK_WEAPONS_PLAYER = CATEGORY_ENEMIES
			| CATEGORY_WEAPONS_ENEMIES | CATEGORY_WALL;

	/**
	 * The mask of weapons of enemies.
	 */
	public static final short MASK_WEAPONS_ENEMIES = CATEGORY_PLAYER
			| CATEGORY_WEAPONS_PLAYER | CATEGORY_WALL;
	
	/**
	 * The mask of the wall.
	 */
	public static final short MASK_WALL = CATEGORY_ENEMIES
			| CATEGORY_WEAPONS_PLAYER | CATEGORY_WEAPONS_ENEMIES
			| CATEGORY_BONUS;
	
	/**
	 * The mask of bonus.
	 */
	public static final short MASK_BONUS = CATEGORY_PLAYER | CATEGORY_WALL;

}
