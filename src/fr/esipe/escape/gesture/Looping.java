package fr.esipe.escape.gesture;

import java.util.List;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;


import fr.esipe.escape.game.spaceship.Deplacement;
import fr.esipe.escape.game.spaceship.Taille;

public class Looping extends Gesture {

	public Looping(NameGesture name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	/**
	 * This methode check if all vector are included between two circles.
	 * @param listVector contains every points where mouse was detected.
	 * @return true if listVector corresponds to a circle
	 */
	public boolean calculateCircle(List<Vec2> listVector){
		if(listVector.size()<10) return false;
		Vec2 first=listVector.get(0);
		boolean valid=false;
		for(int i=0;i<listVector.size();i++){
			for(int j=listVector.size()-1;j>=0;j--)
				if(MathUtils.distance(listVector.get(i),listVector.get(j))<3){
					if((j-i)>=listVector.size()/2){
						valid=true;
						break;
					}
				}
		}

		if(!valid) return false;

		float minY=first.y;Vec2 lowerVec=first;
		float maxY=first.y; Vec2 higherVec=first;
		float minX=first.x;Vec2 leftVec=first;
		float maxX=first.x; Vec2 rightVec=first;
		for(Vec2 v:listVector){
			if(v.y<minY){
				minY=v.y;
				lowerVec=v;
			}
			if(v.y>maxY){
				maxY=v.y;
				higherVec=v;
			}
			if(v.x<minX){
				minX=v.x;
				leftVec=v;
			}
			if(v.x>maxX){
				maxX=v.x;
				rightVec=v;
			}
		}

		float distanceY=MathUtils.distance(lowerVec, higherVec);
		float distanceX=MathUtils.distance(leftVec, rightVec);

		float mindistanceY=leftVec.y;
		if(leftVec.y>rightVec.y) mindistanceY=rightVec.y;
		Vec2 center=new Vec2(leftVec.x+((rightVec.x-leftVec.x)/2),mindistanceY+(Math.abs(leftVec.y-rightVec.y)/2));


		float mindistanceX=lowerVec.x;
		if(higherVec.x<lowerVec.x)mindistanceX=higherVec.x;
		Vec2 center2=new Vec2(mindistanceX+(Math.abs(lowerVec.x-higherVec.x)/2),lowerVec.y+((higherVec.y-lowerVec.y)/2));

		if(distanceX<distanceY){
			float rayon=((30*distanceX)/100)/2;
			float rayon2=(float) ((2*distanceY))/2;
			for(Vec2 vec : listVector){
				if((Math.pow(vec.x-center.x,2)+Math.pow(vec.y-center.y,2))<rayon*rayon) return false;
				if((Math.pow(vec.x-center2.x,2)+Math.pow(vec.y-center2.y,2))>rayon2*rayon2) return false;

			}
		}
		else {
			float rayon2=((30*distanceY)/100)/2;
			float rayon=(float) (2*distanceX)/2;
			for(Vec2 vec : listVector){
				if((Math.pow(vec.x-center.x,2)+Math.pow(vec.y-center.y,2))>rayon*rayon) return false;
				if((Math.pow(vec.x-center2.x,2)+Math.pow(vec.y-center2.y,2))<rayon2*rayon2) return false;

			}
		}		
		return true;
	}

	@Override
	public void calculIntensity(List<Vec2> listVector, Vec2 posInitial) {
		setIntensity(0);		
	}

	

	@Override
	public Deplacement calculForce(Vec2 position, float intensity, Taille element) {
		return new Deplacement(this,new Vec2(0,0),new Vec2(0,0),new Vec2(0,0));
	}

	@Override
	public boolean completeGesture(Deplacement deplacement,Vec2 positionInitial, Vec2 positionC) {
		// TODO Auto-generated method stub
		return false;
	}

}
