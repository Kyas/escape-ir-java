package fr.esipe.escape.gesture;

import java.util.List;

import org.jbox2d.common.Vec2;

/**
 * Allowed to identify correct gesture.
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 *
 */
public class FactoryGesture {

		
	/**
	 * Identification of gesture corresponding to thr list.
	 * @param listVector contains every points where mouse was detected.
	 * @return appropriate Gesture
	 */
	public static Gesture GestureFactory(List<Vec2> listVector){
		if(new LeftOrRight().detectGesture(listVector)) return new LeftOrRight();
		if(new LeftDrift().detectGesture(listVector)) return new LeftDrift();
		if(new RightDrift().detectGesture(listVector)) return new RightDrift();
		if(new LeftLooping().detectGesture(listVector)) return new LeftLooping();
		if(new RightLooping().detectGesture(listVector)) return new RightLooping();
		if(new Backoff().detectGesture(listVector)) return new Backoff();
		if(new Move().detectGesture(listVector)) return new Move();
		return null;
	}

}
