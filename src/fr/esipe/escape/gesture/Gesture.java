package fr.esipe.escape.gesture;

import java.util.List;

import org.jbox2d.common.Vec2;

import fr.esipe.escape.game.spaceship.Deplacement;
import fr.esipe.escape.game.spaceship.Taille;


public abstract class Gesture {

	/**
	 * Listing of gesture
	 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
	 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
	 *
	 */
	public enum NameGesture {
		ACTIVEARME,LEFTORRIGHT,MOVE,LEFT_DRIFT,RIGHT_DRIFT,LEFTLOOPING,RIGTH_LOOPING,BACKOFF,UNKWNON
	}
	
	private final NameGesture name;
	private float intensity=0;
	
	public Gesture(NameGesture name){
		this.name=name;
		
	}
	
	public abstract void calculIntensity(List<Vec2> listVector, Vec2 posInitial);

	public abstract Deplacement calculForce(Vec2 posInit, float intensity,Taille element);
	
	public abstract boolean completeGesture(Deplacement deplacement,Vec2 positionfinal, Vec2 positionC);
	
	
	public float getIntensity() {
		return intensity;
	}

	public void setIntensity(float intensity) {
		this.intensity = intensity;
	}

	public NameGesture getName() {
		return name;
	}

	
	
}
