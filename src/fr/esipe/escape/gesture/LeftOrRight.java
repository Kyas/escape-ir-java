package fr.esipe.escape.gesture;

import java.util.List;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.game.spaceship.Deplacement;
import fr.esipe.escape.game.spaceship.Taille;


/**
 * This class implements Gesture implementation. It allows to detect LeftOrRight Gesture.
 * LeftOrRigh corresponds to a long pressure of the mouse at the same place.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 *
 */
public class LeftOrRight extends Gesture implements IGesture{

	public LeftOrRight() {
		super(NameGesture.LEFTORRIGHT);
	}

	@Override
	public boolean detectGesture(List<Vec2> listVector) {
		if(listVector.isEmpty()) return false;
		for(int i=0;i<listVector.size();i++){
			for(int j=0;j<listVector.size();j++){
				if(MathUtils.distance(listVector.get(i), listVector.get(j))>2) return false;
			}
		}
		return true;
	}

	@Override
	public void calculIntensity(List<Vec2> listVector, Vec2 posInitial) {
		float intensity;
		if(posInitial.x<listVector.get(0).x){
			intensity=listVector.size();
		}
		else{
			intensity=listVector.size()*(-1);
		}
		setIntensity(intensity);
	}


	@Override
	public Deplacement calculForce(Vec2 posInit, float intensity, Taille element) {
		Vec2 force=null;
		Deplacement dep;
		float posXFinal;
		if(intensity<0){
			force=new Vec2(-50.0f, 0.0f);
			float calculProgression=Math.abs(intensity)*30;
			posXFinal=posInit.x-calculProgression;
		}
		else{
			force=new Vec2(50.0f, 0.0f);
			float calculProgression=intensity*30;
			posXFinal=posInit.x+calculProgression;
			
		}
		Vec2 posFinal=new Vec2(posXFinal,posInit.y);
		dep=new Deplacement(this, posInit, posFinal, force);
		return dep;
	}
	

	@Override
	public boolean completeGesture(Deplacement deplacement,Vec2 posInitial, Vec2 positionC) {
		if((deplacement.getPosFinal().x-deplacement.getPosInitial().x)<0){
			float length=deplacement.getPosFinal().x-deplacement.getPosInitial().x;
			float length2=positionC.x-posInitial.x;
			if(length2<=length) return true;
			return false;
		}
		else{
			float length=deplacement.getPosFinal().x-deplacement.getPosInitial().x;
			float length2=positionC.x-posInitial.x;
			if(length2>=length) return true;
			return false;
		}
	}

	
}
