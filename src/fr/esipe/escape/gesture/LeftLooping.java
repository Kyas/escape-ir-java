package fr.esipe.escape.gesture;

import java.util.List;

import org.jbox2d.common.Vec2;

/**
 * This class implements Gesture implementation. It allows to detect LeftLooping Gesture.
 * LeftLooping corresponds to a line describing a circle in anticlockwise.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 *
 */
public class LeftLooping extends Looping implements IGesture{

	public LeftLooping() {
		super(NameGesture.LEFTLOOPING);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean detectGesture(List<Vec2> listVector) {
		if(listVector.isEmpty()) return false;
		if(listVector.get(listVector.size()/4).x<listVector.get((listVector.size()/4)*3).x) return false;
		return calculateCircle(listVector);
	}
	
}
