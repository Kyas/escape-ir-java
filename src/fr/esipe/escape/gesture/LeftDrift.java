package fr.esipe.escape.gesture;

import java.util.List;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.Set;
import fr.esipe.escape.game.spaceship.Deplacement;
import fr.esipe.escape.game.spaceship.Taille;

/**
 * This class implements Gesture implementation. It allows to detect LeftDrift Gesture.
 * LeftDrigt corresponds to a line from the bottom to the high in diagonale to the left
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 *
 */
public class LeftDrift extends Gesture implements IGesture {

	public LeftDrift() {
		super(NameGesture.LEFT_DRIFT);
	}

	@Override
	public  boolean detectGesture(List<Vec2> listVector) {
		
		if(listVector.size()<10) return false;
		
		Vec2 first=listVector.get(0);
		Vec2 last=listVector.get(listVector.size()-1);
	
		if((first.x-last.x)<(3*Set.getSet().getHeight()/100)) return false;
		
		if(first.x<last.x||first.y<last.y) return false;
		
		
		Vec2 lX=new Vec2(last.x-(2*Set.getSet().getHeight()/100),last.y);
		if(!lX.isValid()){
			lX=new Vec2(0,last.y);
		}
		
		
		Vec2 hY=new Vec2(last.x,last.y-(2*Set.getSet().getWidth()/100));
		if(!hY.isValid()){
			hY=new Vec2(last.x,0);
		}
	
		Vec2 rX=new Vec2(first.x+(2*Set.getSet().getHeight()/100),first.y);
		if(!rX.isValid()){
			rX=new Vec2(Set.getSet().getHeight(),first.y);
		}
		
		Vec2 bY=new Vec2(first.x,first.y+(2*Set.getSet().getWidth()/100));
		if(!bY.isValid()){
			bY=new Vec2(first.x,Set.getSet().getWidth());
		}
		
		for(Vec2 vec : listVector){
			if(vec.y<hY.y||vec.y>bY.y||vec.x<lX.x||vec.x>rX.x) return false;
		}
		
		return true;
	}

	@Override
	public void calculIntensity(List<Vec2> listVector, Vec2 posInitial) {
		setIntensity( MathUtils.distance(listVector.get(0), listVector.get(listVector.size()-1)));
	}

	@Override
	public Deplacement calculForce(Vec2 posInit, float intensity,Taille element) {	
		Vec2 force=new Vec2(-50.0f, -40.0f);
		float calculProgressionY=(intensity*60)/100;
		float calculProgressionX=(intensity*60)/100;
		float posYFinal=posInit.y-calculProgressionY;
		float posXFinal=posInit.x-calculProgressionX;	
		Vec2 posFinal=new Vec2(posXFinal,posYFinal);
		return new Deplacement(this,posInit,posFinal,force);
	}

	@Override
	public boolean completeGesture(Deplacement deplacement, Vec2 posInit , Vec2 positionC) {
		float lengthY=deplacement.getPosFinal().y-deplacement.getPosInitial().y;
		float lengthY2=positionC.y-posInit.y;
		float lengthX=deplacement.getPosFinal().x-deplacement.getPosInitial().x;
		float lengthX2=positionC.x-posInit.x;
		if(lengthY2<=lengthY || lengthX2<=lengthX) return true;
		return false;
	}


	
}
