package fr.esipe.escape.gesture;

import java.util.List;

import org.jbox2d.common.MathUtils;
import org.jbox2d.common.Vec2;

import fr.esipe.escape.Set;
import fr.esipe.escape.game.spaceship.Deplacement;
import fr.esipe.escape.game.spaceship.Taille;


/**
 * This class implements Gesture implementation. It allows to detect Move Gesture.
 * Move corresponds to a line from the bottom to the high.
 * 
 * @author C�line P�rillous <cperillous@etudiant.univ-mlv.fr>
 * @author J�r�my Lor <jlor@etudiant.univ-mlv.fr>
 *
 */
public class Move extends Gesture implements IGesture {


	public Move() {
		super(NameGesture.MOVE);
	}

	@Override
	public boolean detectGesture(List<Vec2> listVector) {

		if(listVector.isEmpty()) return false;
		Vec2 first=listVector.get(0);
		Vec2 last=listVector.get(listVector.size()-1);
		if(Math.abs(first.x-last.x)>(3*Set.getSet().getHeight()/100)||last.y>first.y) return false;
		float minX=last.x,maxX=first.x;
		if(first.x<last.x){
			minX=first.x;
			maxX=last.x;
		}
		
		for(Vec2 vec : listVector){
			if(vec.x<(minX-(2*Set.getSet().getHeight()/100))) return false;
			if(vec.x>(maxX+(2*Set.getSet().getHeight()/100))) return false;
			if(vec.y<last.y||vec.y>first.y) return false;
		}
		return true;
	}

	@Override
	public void calculIntensity(List<Vec2> listVector, Vec2 posInitial) {
		setIntensity(MathUtils.distance(listVector.get(0), listVector.get(listVector.size()-1)));
	}


	@Override
	public Deplacement calculForce(Vec2 posInit, float intensity, Taille element) {
		Vec2 force=new Vec2(0.0f, -30.0f);
		float calculProgression=(intensity*60)/100;
		float posYFinal=posInit.y-calculProgression;
		Vec2 posFinal=new Vec2(posInit.x,posYFinal);
		return new Deplacement(this,posInit,posFinal,force);
	}

	@Override
	public boolean completeGesture(Deplacement deplacement,Vec2 posInit, Vec2 positionC) {
		float length=deplacement.getPosFinal().y-deplacement.getPosInitial().y;
		float length2=positionC.y-posInit.y;
		if(length2<=length) return true;
		return false;
	}



}
