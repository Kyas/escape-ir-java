package fr.esipe.escape;

public class Set {

	private final long height;
	private final long width;
	
	private static Set singleton;
	
	private Set(long height, long width){
		this.height=height;
		this.width=width;
	}
	
	
	public static void createSet(long height, long width){
		if(singleton==null){
			singleton=new Set(height,width);
		}
	}
	
	public  static Set getSet(){
		if(singleton!=null){
			return singleton;
		}
		else {
			createSet(NewMainClass.HEIGHT, NewMainClass.WIDTH);
		}
		return singleton;
	}


	public long getHeight() {
		return height;
	}


	public long getWidth() {
		return width;
	}
		
}

