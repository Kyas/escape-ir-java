package fr.esipe.escape.level;

import java.awt.Image;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

import fr.esipe.escape.Set;
import fr.esipe.escape.game.spaceship.Player;
import fr.esipe.escape.game.spaceship.boss.AbstractBoss;
import fr.esipe.escape.game.spaceship.enemy.SpaceShipEnemy;

public class Level {
	
	private final int identifiant;
	private final LinkedList<SpaceShipEnemy>[] enemies;
	private final AbstractBoss boss;
	private int nbLive;
	private final Image background;
	private final int[] weapons;
	private final Player player;
	
	
	public Level(int identifiant,LinkedList<SpaceShipEnemy>[] enemies,AbstractBoss boss, int nbLive, Image background, int[] weapons) {
		this.identifiant = identifiant;
		this.enemies = enemies;
		this.boss = boss;
		this.nbLive = nbLive;
		this.background = background;
		this.weapons = weapons;
		this.player=new Player((int)(Set.getSet().getWidth()/2)-23,(int)(Set.getSet().getHeight()*2/3)+31, nbLive);
	}

	public Player getPlayer(){
		return player;
	}
	
	public void lostLive(){
		nbLive--;
	}
	
	
	public int getNbLive() {
		return nbLive;
	}


	public int getIdentifiant() {
		return identifiant;
	}

	
	public Image getBackground() {
		return background;
	}


	public ArrayList<SpaceShipEnemy> selectionEnnemis(){
		Random r=new Random();
		ArrayList<SpaceShipEnemy> arrayList=new ArrayList<>();
		int typeEnemy=-1;
		boolean finish=true;
		for(LinkedList<SpaceShipEnemy> list:enemies){
			finish&=list.isEmpty();
		}
		if(finish==true){
			return null;
		}
		while(typeEnemy==-1||enemies[typeEnemy].isEmpty()){
			typeEnemy=Math.abs(r.nextInt()%3);
		}
		int nbSpace=0;
		int tailleInit=enemies[typeEnemy].size();
		while(nbSpace<tailleInit && nbSpace<4){
			arrayList.add(enemies[typeEnemy].get(0));
			enemies[typeEnemy].remove(0);
			nbSpace++;
		}
		return arrayList;
	}

	public AbstractBoss getBoss() {
		return boss;
	}

	
		
		
}
