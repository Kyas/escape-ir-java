package fr.esipe.escape.level;

import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.esipe.escape.Set;
import fr.esipe.escape.game.spaceship.boss.AbstractBoss;
import fr.esipe.escape.game.spaceship.boss.Boss1;
import fr.esipe.escape.game.spaceship.boss.Boss2;
import fr.esipe.escape.game.spaceship.boss.Boss3;
import fr.esipe.escape.game.spaceship.enemy.SpaceShipEnemy;
import fr.esipe.escape.ui.drawable.ImageFactory;

/**
 * 
 */
public class XMLLoader {

	public static List<Level> loadLevel() throws Exception {
		List<Level> levels = new ArrayList<Level>();
		try { /* Ouverture du document XML, si erreur lancement Exception */
			DocumentBuilderFactory fabrique = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder constructeur = fabrique.newDocumentBuilder();
			File xml = new File("conf/configuration.xml");
			Document document = constructeur.parse(xml);
			if (!document.hasChildNodes()) {
				throw new Exception();
			} else {
				/* Selection element Racine */
				Element racine = document.getDocumentElement();
				/*
				 * Test si l'element Racine possede des noeuds fils , sinon
				 * lancement exception
				 */
				if (!racine.hasChildNodes()) {
					throw new Exception();
				} else {
					/* Recuperation des noeuds Test */
					NodeList liste = racine.getElementsByTagName("level");
					levels.add(readLevel(liste.item(0), racine, 0));

					// liste = racine.getElementsByTagName("level2");
					levels.add(readLevel(liste.item(0), racine, 1));

					// liste = racine.getElementsByTagName("level3");
					levels.add(readLevel(liste.item(0), racine, 2));

				}
			}

		} catch (Throwable e) {
			System.err.println("Initialization Error");
		}
		return levels;
	}

	private static Level readLevel(Node node, Element e, int id) {

		Image img;
		@SuppressWarnings("unchecked")
		LinkedList<SpaceShipEnemy>[] enemies = (LinkedList<SpaceShipEnemy>[]) new LinkedList<?>[3];
		enemies[0] = new LinkedList<>();
		enemies[1] = new LinkedList<>();
		enemies[2] = new LinkedList<>();
		AbstractBoss boss;
		int nbLive;
		int identifiant;
		int[] weapons = new int[4];
		try {
			identifiant = id;
			Node l = e.getElementsByTagName("image").item(id);
			img = ImageFactory.getImage(((Element) l).getAttribute("path"));
			l = e.getElementsByTagName("life").item(id);
			;
			nbLive = Integer.parseInt(((Element) l).getAttribute("nombre"));
			l = e.getElementsByTagName("enemy1").item(id);
			;
			int nb = Integer.parseInt(((Element) l).getAttribute("nombre"));
			enemies[0] = BuildWave.buildEnemy1(nb);
			l = e.getElementsByTagName("enemy2").item(id);
			nb = Integer.parseInt(((Element) l).getAttribute("nombre"));
			enemies[1] = BuildWave.buildEnemy2(nb);
			l = e.getElementsByTagName("enemy3").item(id);
			nb = Integer.parseInt(((Element) l).getAttribute("nombre"));
			enemies[2] = BuildWave.buildEnemy3(nb);

			l = e.getElementsByTagName("boss").item(id);
			int categorie = (Integer.parseInt(((Element) l)
					.getAttribute("categorie")));
			if (categorie == 1) {
				boss = new Boss1((int) (Set.getSet().getWidth() / 2) - (84),
						(int) Set.getSet().getHeight() / 4);
			} else if (categorie == 2) {
				boss = new Boss2((int) (Set.getSet().getWidth() / 2) - (87),
						(int) Set.getSet().getHeight() / 4);
			} else if (categorie == 3) {
				boss = new Boss3((int) (Set.getSet().getWidth() / 2) - (81),
						(int) Set.getSet().getHeight() / 4);
			} else
				throw new IllegalStateException();
			l = e.getElementsByTagName("missile").item(id);
			;
			weapons[0] = Integer.parseInt(((Element) l).getAttribute("nombre"));
			l = e.getElementsByTagName("fireball").item(id);
			;
			weapons[1] = Integer.parseInt(((Element) l).getAttribute("nombre"));
			l = e.getElementsByTagName("shiboleet").item(id);
			;
			weapons[2] = Integer.parseInt(((Element) l).getAttribute("nombre"));
			l = e.getElementsByTagName("lazer").item(id);
			weapons[3] = Integer.parseInt(((Element) l).getAttribute("nombre"));

			return new Level(identifiant, enemies, boss, nbLive, img, weapons);

		} catch (Exception ex) {
			System.err.println("Error of reading of the XML File :" + ex);
			System.out.println("Erreur lecture fichier XML :" + ex);
		}
		return null;
	}

}