package fr.esipe.escape.level;

import java.util.LinkedList;

import fr.esipe.escape.Set;
import fr.esipe.escape.game.spaceship.enemy.EnemyHard;
import fr.esipe.escape.game.spaceship.enemy.EnemyMedium;
import fr.esipe.escape.game.spaceship.enemy.EnemySimple;
import fr.esipe.escape.game.spaceship.enemy.SpaceShipEnemy;

public class BuildWave {

	public static LinkedList<SpaceShipEnemy> buildEnemy1(int nbEnemy){
		LinkedList<SpaceShipEnemy> list=new LinkedList<>();
		int nbVaisseau=nbEnemy;
		int nbVague=nbEnemy/4;
		int espace=(int)(Set.getSet().getHeight()/4);
		for(int i=1;i<=nbVague;i++){
			for(int j=0;j<4;j++){
				list.add(new EnemySimple((j*espace)+20, 0));
				nbVaisseau--;
			}
		}

		for(int i=0;i<nbVaisseau;i++){
			list.add(new EnemySimple((i*espace)+50*(4-nbVaisseau), 0));
		}
		return list;
	}

	public static LinkedList<SpaceShipEnemy> buildEnemy2(int nbEnemy) {
		LinkedList<SpaceShipEnemy> list=new LinkedList<>();
		int nbVaisseau=nbEnemy;
		int nbVague=nbEnemy/4;
		int espace=(int)(Set.getSet().getHeight()/4);
		for(int i=1;i<=nbVague;i++){
			for(int j=0;j<4;j++){
				list.add(new EnemyMedium((j*espace)+20, 0));
				nbVaisseau--;
			}
		}
		for(int i=0;i<nbVaisseau;i++){
			list.add(new EnemyMedium((i*espace)+50*(4-nbVaisseau), 0));
		}
		return list;
	}

	public static LinkedList<SpaceShipEnemy> buildEnemy3(int nbEnemy) {
		LinkedList<SpaceShipEnemy> list=new LinkedList<>();
		int nbVaisseau=nbEnemy;
		int nbVague=nbEnemy/4;
		int espace=(int)(Set.getSet().getHeight()/4);
		for(int i=1;i<=nbVague;i++){
			for(int j=0;j<4;j++){
				list.add(new EnemyHard((j*espace)+20, 0));
				nbVaisseau--;
			}
		}
		for(int i=0;i<nbVaisseau;i++){
			list.add(new EnemyHard((i*espace)+50*(4-nbVaisseau), 0));
		}
		return list;
	}
}
