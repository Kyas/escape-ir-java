package fr.esipe.escape.level;


import java.util.ArrayList;

public class BuildLevel {

	private final ArrayList<Level> levels;
	private final int nbLevel;

	public BuildLevel() throws Exception {
		levels=(ArrayList<Level>) XMLLoader.loadLevel();
		nbLevel=levels.size();
	}

	public Level getLevel(int nmLevel){
		if(nmLevel>0 && nmLevel<=nbLevel){
			return levels.get(nmLevel-1);
		}
		else {throw new IllegalArgumentException();

		}
	}

	public int getNblevel() {
		return nbLevel;
	}


}
